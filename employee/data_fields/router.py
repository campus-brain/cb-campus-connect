from typing import Optional

from fastapi import APIRouter, Depends, Header, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from employee.data_fields import data_fields
from utils import schemas,models
from utils import database
from utils.database import get_db

data_fields_router = APIRouter(tags=['Admin - Employee - Data Fields'], prefix='')

@data_fields_router.get("/fields",response_model=schemas.DataField)
def get_employee_fields(db:Session = Depends(database.get_db)):
    """
    Get all Employee fields of the school
    """
    return data_fields.get_employee_fields(db)

@data_fields_router.post("/sections/update",response_model=schemas.DataField)
def update_employee_section(request:schemas.SectionUpdate,db:Session = Depends(database.get_db)):
    """
    Update Employee section of the school
    """
    return data_fields.update_employee_section(request,db)

@data_fields_router.post("/fields/update",response_model=schemas.DataField)
def update_employee_fields(request:schemas.DataFieldUpdate,db:Session = Depends(database.get_db)):
    """
    Update Employee fields of the school
    """
    return data_fields.update_employee_fields(request,db)
