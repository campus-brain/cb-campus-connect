from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from utils import schemas, models
from utils import database, data_fields


def get_employee_fields(db:Session):
    return data_fields.get_fields('employee',db)

def update_employee_fields(request:schemas.DataFieldUpdate,db:Session):
    table_name = request.section.id
    if request.section.id not in ["Employee"]:
        table_name = "EmployeeAdditionalFields"
    return data_fields.update_fields('employee',table_name,request,db)



def update_employee_section(request:schemas.SectionUpdate,db:Session):
    table_name = None
    is_additional_table = False
    if request.old_record:
        table_name = request.old_record.id
    if table_name and (table_name not in ["Employee"]):
        table_name = "EmployeeAdditionalFields"
        is_additional_table = True
    return data_fields.update_sections('employee', table_name, request, is_additional_table ,db)
   