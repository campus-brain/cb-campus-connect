from sqlalchemy import JSON, Boolean, Column, Integer, String

from utils.database import Base


class StaffDesignations(Base):
    __tablename__ = 'StaffDesignations'

    designation_id = Column('designationID',Integer, primary_key=True, index=True)
    designation_name = Column('designationName',String(56), unique=True)
