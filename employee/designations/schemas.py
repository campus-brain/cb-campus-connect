from typing import List, Optional, Union

from humps import camelize
from pydantic import BaseModel, Json


class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class Status(CamelModel):
    success:bool

class StaffDesignations(CamelModel):
    designation_id:int
    designation_name:str
    class Config:
        orm_mode=True

class StaffDesignationsResponse(CamelModel):
    designations: List[StaffDesignations]
    class Config:
        orm_mode=True


class StaffDesignationsPost(CamelModel):
    designation_name:str
    class Config:
        orm_mode=True
