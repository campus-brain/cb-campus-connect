from typing import Optional

from fastapi import APIRouter, Depends, Header, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from employee.designations import models, schemas, designations
from utils import database
from utils.database import get_db

designations_router = APIRouter(tags=['Admin - Employee - Designations'], prefix='/designations')

@designations_router.get("/",response_model=schemas.StaffDesignationsResponse)
def get_all_designations(db:Session = Depends(database.get_db)):
    """
    Get all designations
    """
    return designations.get_all_designations(db)

@designations_router.post("/",response_model=schemas.StaffDesignations)
def create_designation(request:schemas.StaffDesignationsPost,db:Session = Depends(database.get_db)):
    """
    Create designation
    """
    return designations.create_designation(request,db)

@designations_router.patch("/{designation_id}/",response_model=schemas.StaffDesignations)
def edit_designation(designation_id:int,request:schemas.StaffDesignationsPost,db:Session = Depends(database.get_db)):
    """
    Edit designation
    """
    return designations.edit_designation(designation_id,request,db)

@designations_router.delete("/{designation_id}/",response_model=schemas.Status)
def delete_designation(designation_id:int,db:Session = Depends(database.get_db)):
    """
    Delete designation
    """
    return designations.delete_designation(designation_id,db)