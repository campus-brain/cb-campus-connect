from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError

from employee.designations import models, schemas
from utils import database

def get_all_designations(db:Session):
    return {"designations": db.query(models.StaffDesignations).all()}

def create_designation(request:schemas.StaffDesignationsPost,db:Session):
    try:
        new_designation = models.StaffDesignations(designation_name=request.designation_name)
        db.add(new_designation)
        db.commit()
        db.refresh(new_designation)
        return new_designation
    except IntegrityError:
        raise HTTPException(status_code=400,detail="Duplicate Column")

def edit_designation(designation_id:int,request:schemas.StaffDesignationsPost,db:Session):
    result_copy = db.query(models.StaffDesignations).filter(models.StaffDesignations.designation_id == designation_id)
    result = result_copy.first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"designation Not Found")
    result_copy.update({"designation_name": request.designation_name})
    db.commit()
    db.refresh(result)
    return result

def delete_designation(designation_id:int,db:Session):
    try:
        db.query(models.StaffDesignations).filter(models.StaffDesignations.designation_id==designation_id).delete()
        db.commit()
        return {"success":True}
    except Exception as e:
        print(e)
        raise HTTPException(status_code=400,detail="Unable to delete the row")