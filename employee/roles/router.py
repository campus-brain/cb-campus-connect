from typing import Optional

from fastapi import APIRouter, Depends, Header, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from employee.roles import models, schemas, roles
from utils import database
from utils.database import get_db

roles_router = APIRouter(tags=['Admin - Employee - Roles'], prefix='/roles')

@roles_router.get("/",response_model=schemas.StaffRolesResponse)
def get_all_roles(db:Session = Depends(database.get_db)):
    """
    Get all roles
    """
    return roles.get_all_roles(db)

@roles_router.post("/",response_model=schemas.StaffRoles)
def create_role(request:schemas.StaffRolesPost,db:Session = Depends(database.get_db)):
    """
    Create Role
    """
    return roles.create_role(request,db)

@roles_router.patch("/{role_id}/",response_model=schemas.StaffRoles)
def edit_role(role_id:int,request:schemas.StaffRolesPost,db:Session = Depends(database.get_db)):
    """
    Edit Role
    """
    return roles.edit_role(role_id,request,db)

@roles_router.delete("/{role_id}/",response_model=schemas.Status)
def delete_role(role_id:int,db:Session = Depends(database.get_db)):
    """
    Delete Role
    """
    return roles.delete_role(role_id,db)