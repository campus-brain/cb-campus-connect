from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError

from employee.roles import models, schemas
from utils import database

def get_all_roles(db:Session):
    return {"roles": db.query(models.StaffRoles).all()}

def create_role(request:schemas.StaffRolesPost,db:Session):
    try:
        new_role = models.StaffRoles(role_name=request.role_name,editable=True)
        db.add(new_role)
        db.commit()
        db.refresh(new_role)
        return new_role
    except IntegrityError:
        raise HTTPException(status_code=400,detail="Duplicate Role")

def edit_role(role_id:int,request:schemas.StaffRolesPost,db:Session):
    result_copy = db.query(models.StaffRoles).filter(models.StaffRoles.role_id == role_id)
    result = result_copy.first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Role Not Found")
    result_copy.update({"role_name": request.role_name})
    db.commit()
    db.refresh(result)
    return result

def delete_role(role_id:int,db:Session):
    try:
        db.query(models.StaffRoles).filter(models.StaffRoles.role_id==role_id).delete()
        db.commit()
        return {"success":True}
    except Exception as e:
        print(e)
        raise HTTPException(status_code=400,detail="Unable to delete the row")