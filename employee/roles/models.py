from sqlalchemy import JSON, Boolean, Column, Integer, String

from utils.database import Base


class StaffRoles(Base):
    __tablename__ = 'StaffRoles'

    role_id = Column('roleID', Integer, primary_key=True, index=True)
    role_name = Column('RoleName', String(56), unique=True)
    editable = Column('Editable', Boolean)
