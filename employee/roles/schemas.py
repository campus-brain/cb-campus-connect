from typing import List, Optional, Union

from humps import camelize
from pydantic import BaseModel, Json


class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class Status(CamelModel):
    success:bool

class StaffRoles(CamelModel):
    role_id:int
    role_name:str
    editable:bool
    class Config:
        orm_mode=True

class StaffRolesResponse(CamelModel):
    roles: List[StaffRoles]
    class Config:
        orm_mode=True


class StaffRolesPost(CamelModel):
    role_name:str
    class Config:
        orm_mode=True
