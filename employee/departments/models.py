from sqlalchemy import JSON, Boolean, Column, Integer, String

from utils.database import Base


class StaffDepartments(Base):
    __tablename__ = 'StaffDepartments'

    dept_id = Column('DeptID',Integer, primary_key=True, index=True)
    dept_name = Column('DeptName',String(56), unique=True)
