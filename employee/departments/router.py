from typing import Optional

from fastapi import APIRouter, Depends, Header, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from employee.departments import models, schemas, departments
from utils import database
from utils.database import get_db

departments_router = APIRouter(tags=['Admin - Employee - Departments'], prefix='/departments')

@departments_router.get("/",response_model=schemas.StaffDepartmentsResponse)
def get_all_departments(db:Session = Depends(database.get_db)):
    """
    Get all departments
    """
    return departments.get_all_departments(db)

@departments_router.post("/",response_model=schemas.StaffDepartments)
def create_department(request:schemas.StaffDepartmentsPost,db:Session = Depends(database.get_db)):
    """
    Create department
    """
    return departments.create_department(request,db)

@departments_router.patch("/{dept_id}/",response_model=schemas.StaffDepartments)
def edit_department(dept_id:int,request:schemas.StaffDepartmentsPost,db:Session = Depends(database.get_db)):
    """
    Edit department
    """
    return departments.edit_department(dept_id,request,db)

@departments_router.delete("/{dept_id}/",response_model=schemas.Status)
def delete_department(dept_id:int,db:Session = Depends(database.get_db)):
    """
    Delete department
    """
    return departments.delete_department(dept_id,db)