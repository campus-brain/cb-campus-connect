from typing import List, Optional, Union

from humps import camelize
from pydantic import BaseModel, Json


class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class Status(CamelModel):
    success:bool

class StaffDepartments(CamelModel):
    dept_id:int
    dept_name:str
    class Config:
        orm_mode=True

class StaffDepartmentsResponse(CamelModel):
    departments: List[StaffDepartments]
    class Config:
        orm_mode=True


class StaffDepartmentsPost(CamelModel):
    dept_name:str
    class Config:
        orm_mode=True
