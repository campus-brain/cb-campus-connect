from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError

from employee.departments import models, schemas
from utils import database

def get_all_departments(db:Session):
    return {"departments": db.query(models.StaffDepartments).all()}

def create_department(request:schemas.StaffDepartmentsPost,db:Session):
    try:
        new_department = models.StaffDepartments(dept_name=request.dept_name)
        db.add(new_department)
        db.commit()
        db.refresh(new_department)
        return new_department
    except IntegrityError:
        raise HTTPException(status_code=400,detail="Duplicate Column")

def edit_department(dept_id:int,request:schemas.StaffDepartmentsPost,db:Session):
    result_copy = db.query(models.StaffDepartments).filter(models.StaffDepartments.dept_id == dept_id)
    result = result_copy.first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Department Not Found")
    result_copy.update({"dept_name": request.dept_name})
    db.commit()
    db.refresh(result)
    return result

def delete_department(dept_id:int,db:Session):
    try:
        db.query(models.StaffDepartments).filter(models.StaffDepartments.dept_id==dept_id).delete()
        db.commit()
        return {"success":True}
    except Exception as e:
        print(e)
        raise HTTPException(status_code=400,detail="Unable to delete the row")