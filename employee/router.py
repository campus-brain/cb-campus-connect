from typing import Optional

from fastapi import APIRouter, Depends, Header, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from employee.roles.router import roles_router
from employee.departments.router import departments_router
from employee.designations.router import designations_router
from employee.data_fields.router import data_fields_router

employee_router = APIRouter(prefix='/admin/employee')
employee_router.include_router(roles_router)
employee_router.include_router(departments_router)
employee_router.include_router(designations_router)
employee_router.include_router(data_fields_router)