from sqlalchemy import JSON, Boolean, Column, Integer, String

from utils.database import Base


class DataFields(Base):
    __tablename__ = 'DataFields'

    id = Column(Integer, primary_key=True, index=True)
    role_name = Column('RoleName',String(56), unique=True)
    sections = Column(JSON)