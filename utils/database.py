from sqlalchemy import create_engine, inspect, MetaData, Table, Column, Integer, ForeignKey, String, Column, text, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session
from decouple import config
from alembic.config import Config
from alembic.migration import MigrationContext
from alembic.operations import Operations
from typing import List
from datetime import datetime
from sqlalchemy.sql import insert, update
from sqlalchemy.sql.sqltypes import Date, DateTime

Base = declarative_base()

def get_db_url():
    database = config('DB_NAME', default='srkm_db', cast=str)
    username = 'admin'
    password = 'StrangeSupreme#4'
    # endpoint = 'cb-dev-db.campusbrain.in'
    endpoint = 'campus-connect.cedygmagznvh.ap-south-1.rds.amazonaws.com'
    database_url = f"mysql+pymysql://{username}:{password}@{endpoint}/{database}"
    return database_url

def get_engine():
    database_url = get_db_url()

    engine = create_engine(database_url)
    return engine


def get_db():
    engine = get_engine()

    local_session = sessionmaker(bind=engine, autocommit=False, autoflush=False)
    db = local_session()
    try:
        yield db
    finally:
        db.close()

def get_columns(table_name:str):
    columns = []
    engine = get_engine()
    for col in inspect(engine).get_columns(table_name):
        columns.append(col['name'])
    return columns


def check_if_column_exists(table_name: str, column_name: str):
    has_column = False
    engine = get_engine()
    for col in inspect(engine).get_columns(table_name):
        if column_name in col['name']:
            has_column = True
            break
    
    return has_column

def get_operations():
    engine = get_engine()
    conn = engine.connect()
    context = MigrationContext.configure(conn)
    op = Operations(context)
    return op

def add_columns_in_bulk(table_name:str, columns):
    try:
        op = get_operations()
        with op.batch_alter_table(table_name) as batch_op:
            for column in columns:
                batch_op.add_column(Column(column['id'], String(300)))
        return True
    except Exception as e:
        print(e)
        return False
    

def add_column(table_name: str, column_name: str, length: int):
    try:
        op = get_operations()
        if not length:
            length = 400
        op.add_column(table_name, Column(column_name, String(length)))
        return True
    except Exception as e:
        print(e)
        return False

def insert_record(table_name:str, data, db:Session):
    try:
        table = get_table(table_name)
        record = db.execute(insert(table).values(data))
        print(record.lastrowid)
        db.commit()
        return record.lastrowid
    except Exception as e:
        print("Error while inserting record")
        print(e)
        return False

def drop_column(table_name: str, column_name: str):
    try:
        op = get_operations()
        op.drop_column(table_name, column_name)
        return True
    except Exception as e:
        print(e)
        return False

def update_record_in_bulk(table_name_string:str, records:int, values):
    selected_table = get_table(table_name_string)
    op = get_operations()
    records = [f'\"{record}\"' for record in records]
    record_text = ",".join(records)
    try:
        op.execute(selected_table.update(whereclause=text(f'Id in ({record_text})')).values(values))
        return True
    except Exception as e:
        print(e)
        return False
def update_record(table_name_string:str, record_id:int, values):
    selected_table = get_table(table_name_string)
    op = get_operations()
    try:
        op.execute(selected_table.update(whereclause=text(f'Id = "{record_id}"')).values(values))
        return True
    except Exception as e:
        print(e)
        return False

def drop_multiple_columns(table_name:str,columns:List[str]):
    try:
        op = get_operations()
        # with op.batch_alter_table(table_name) as batch_op:
        #     for column_name in columns:
        #         batch_op.drop_column(table_name, column_name)
        for column_name in columns:
            if check_if_column_exists(table_name, column_name):
                op.drop_column(table_name, column_name)

        return True
    except Exception as e:
        print(e)
        return False

def rename_column(table_name: str, old_column_name: str, new_column_name: str,length_of_data:int = 400):
    try:
        if not length_of_data:
            length_of_data = 400
        op = get_operations()
        op.alter_column(table_name, old_column_name,new_column_name=new_column_name,existing_type=String(length_of_data))
        return True
    except Exception as e:
        print(e)
        return False
        
    

def has_table(table_name):
    engine = get_engine()
    return inspect(engine).has_table(table_name)

def get_meta():
    engine = get_engine()
    schema = config('DB_NAME', default='', cast=str)
    meta = MetaData(engine,schema=schema)
    meta.reflect(bind=engine)
    return meta


def get_table(table_name:str):
    meta = get_meta()
    table = Table(table_name,meta)
    return table
def create_table_for_student(table_name:str):
    try:
        meta = get_meta()
        Table(table_name,meta, Column('id', Integer, primary_key=True),Column('StudentID', String(15), ForeignKey("Student.StudentID"), nullable=False))
        meta.create_all()
        return True
    except Exception as e:
        print(e)
        return False

def create_table_for_admissions(table_name:str, template):
    try:
        meta = get_meta()
        Table(
            table_name, meta, 
            Column('Id', Integer, primary_key=True),
            Column('VisitorID', String(15), nullable=False),
            Column('LoginID', String(15), nullable=False),
            Column('InitiatedAt', DateTime, nullable=False),
            Column('SubmittedAt', DateTime, nullable=True),
            Column('Onboarded', Boolean, default=False,nullable=True),
            Column('PaymentStatus', String(15), nullable=True),
            Column('Stage', String(15), default='Pending', nullable=False),
        )

        meta.create_all()
        try:
            columns = []
            sections = template['sections']
            for section in sections:
                for field in section['fields']:
                    columns.append(field)
            result =  add_columns_in_bulk(table_name,columns)
            if not result:
                delete_table(table_name)
                return False
            return True
        except Exception as e:
            print(e)
            delete_table(table_name)
        return True
    except Exception as e:
        print(e)
        return False

def delete_table(table_name:str):
    try:
        schema = config('DB_NAME', default='', cast=str)
        engine = get_engine()
        meta = MetaData(engine,schema=schema)
        meta.reflect(bind=engine)
        table = meta.tables.get(f"{schema}.{table_name}")
        if table is not None:
            meta.drop_all(engine, [table], checkfirst=True)
            return True
        return False
    except Exception as e:
        print(e)
        return False 

def rename_table(old_table_name:str,new_table_name:str):
    try:
        schema = config('DB_NAME', default='', cast=str)
        engine = get_engine()
        meta = MetaData(engine,schema=schema)
        meta.reflect(bind=engine)
        table = meta.tables.get(f"{schema}.{old_table_name}")
        if table is not None:
            op = get_operations()
            op.rename_table(old_table_name, new_table_name,schema=schema)
            return True
        return False
    except Exception as e:
        print(e)
        return False 
