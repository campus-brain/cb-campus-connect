from passlib.hash import sha256_crypt

class Hash:
    @staticmethod
    def encrypt(password: str):
        return sha256_crypt.hash(password)

    @staticmethod
    def verify(hashed_password, plain_password):
        return sha256_crypt.verify(plain_password, hashed_password)