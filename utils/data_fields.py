from sqlalchemy.orm import Session
from utils import schemas, models, database
from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder

def get_fields(_type:str,db:Session):
    result = db.query(models.DataFields).filter(models.DataFields.role_name == _type).first()
    return result

def get_column_name(x):
    return x["id"]


def update_sections(_type:str,table_name:str, request:schemas.SectionUpdate,is_additional_table:bool,db:Session):
    result_copy = db.query(models.DataFields).filter(models.DataFields.role_name == _type)
    result = result_copy.first()
    if request.action == 'create':
        sections_json = result.sections
        sections_json["sections"].append({"header":request.new_record,"fields":[]})
        result_copy.update({"sections": jsonable_encoder(sections_json)})
        db.commit()
        db.refresh(result)
        return result
    elif request.action == 'delete':
        sections_json = result.sections
        sections_to_delete = list(filter(lambda x: x["header"]["id"] == request.old_record.id, sections_json["sections"]))
        if len(sections_to_delete) != 1:
            raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Section not found in preference")
        section_to_delete = sections_to_delete[0]
        columns_to_delete = list(map(get_column_name, section_to_delete["fields"]))
        resp = database.drop_multiple_columns(table_name,columns_to_delete)
        if not resp:
            raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Not able to delete a columns of the section")
        filter_result = filter(lambda x: x["header"]["id"]!=request.old_record.id, sections_json["sections"])
        sections_json["sections"] = list(filter_result)
        result_copy.update({"sections": jsonable_encoder(sections_json)})
        db.commit()
        db.refresh(result)
        return result
    
    elif request.action == 'update':
        sections_json = result.sections
        filter_matches = filter(lambda x: x["header"]["id"]==request.old_record.id, sections_json["sections"])
        filter_matches_list = list(filter_matches)
        if len(filter_matches_list) != 1:
            raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Section not found in preference")
        item_to_be_updated = filter_matches_list[0]
        new_columns = item_to_be_updated["fields"]
        if request.old_record.id != request.new_record.id and is_additional_table:
            old_columns = item_to_be_updated["fields"]
            new_columns = []
            for column_data in old_columns:
                new_column_name = request.new_record.id + column_data["id"].split(request.old_record.id)[1]
                
                resp = database.rename_column(table_name,column_data["id"],new_column_name,column_data["length"])
                if not resp:
                    raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Not able to delete a data table")
                column_data["id"] = new_column_name
                new_columns.append(column_data)
        idx = sections_json["sections"].index(item_to_be_updated)
        sections_json["sections"][idx]["header"] = request.new_record
        sections_json["sections"][idx]["fields"] = new_columns
        result_copy.update({"sections": jsonable_encoder(sections_json)})
        db.commit()
        db.refresh(result)
        return result
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail="Wrong Action mentioned")
def update_fields(_type:str,table_name:str, request:schemas.DataFieldUpdate,db:Session):
    result_copy = db.query(models.DataFields).filter(models.DataFields.role_name == _type)
    result = result_copy.first()
    if not database.has_table(table_name):
        raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="No data table found")
    if request.action == 'create':
        if database.check_if_column_exists(table_name,request.new_record.id):
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,detail="Duplicate table column")
        resp = database.add_column(table_name,request.new_record.id,request.new_record.length)
        if not resp:
            raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Not able to create the field in table")
        sections_json = result.sections
        filter_result = list(filter(lambda x: x["header"]["id"]==request.section.id, sections_json["sections"]))
        if len(filter_result)!=1:
            raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Section not found in preference")
        idx = sections_json["sections"].index(filter_result[0])
        sections_json["sections"][idx]["fields"].append(request.new_record)
        result_copy.update({"sections": jsonable_encoder(sections_json)})
        db.commit()
        db.refresh(result)
        return result
        
    if request.action == 'delete':
        if not database.check_if_column_exists(table_name,request.old_record.id):
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,detail="Table column does not exist")
        resp = database.drop_column(table_name,request.old_record.id)
        if not resp:
            raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Not able to delete the field in table")
        sections_json = result.sections
        filter_result = list(filter(lambda x: x["header"]["id"]==request.section.id, sections_json["sections"]))
        if len(filter_result) != 1:
            raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Section not found in preference")
        idx = sections_json["sections"].index(filter_result[0])
        filtered_fields = list(filter(lambda x: x["id"]!=request.old_record.id, sections_json["sections"][idx]["fields"]))
        
        sections_json["sections"][idx]["fields"] = filtered_fields
        result_copy.update({"sections": jsonable_encoder(sections_json)})
        db.commit()
        db.refresh(result)
        return result
    
    if request.action == 'update':
        if not database.check_if_column_exists(table_name,request.old_record.id):
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,detail="Table column does not exist")
        if request.old_record.id != request.new_record.id:
            resp = database.rename_column(table_name,request.old_record.id,request.new_record.id,request.new_record.length)
            if not resp:
                raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Not able to rename the field in table")
        sections_json = result.sections
        filter_result = list(filter(lambda x: x["header"]["id"]==request.section.id, sections_json["sections"]))
        if len(filter_result) != 1:
            raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Section not found in preference")
        idx = sections_json["sections"].index(filter_result[0])
        filtered_fields = list(filter(lambda x: x["id"]==request.old_record.id, sections_json["sections"][idx]["fields"]))
        if len(filtered_fields) != 1:
            raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Field not found in preference")
        field_idx = sections_json["sections"][idx]["fields"].index(filtered_fields[0])
        sections_json["sections"][idx]["fields"][field_idx] = request.new_record
        result_copy.update({"sections": jsonable_encoder(sections_json)})
        db.commit()
        db.refresh(result)
        return result

    raise HTTPException(status_code=status.HTTP_502_BAD_GATEWAY,detail="Wrong Action mentioned")
