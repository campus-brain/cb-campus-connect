from typing import List, Optional, Union

from humps import camelize
from pydantic import BaseModel, Json


class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class DataField(CamelModel):
    role_name: str
    sections: dict
    class Config:
        orm_mode=True

class DataSectionHeader(CamelModel):
    id: str
    name: str
    editable: bool

class SectionFields(CamelModel):
    id: str
    name: str
    type: str
    default: Union[str,bool]
    options: List[str]
    length: Optional[int]
    required: bool
    template_options: dict

class DataFieldUpdate(CamelModel):
    action: str
    section: DataSectionHeader
    old_record: Optional[SectionFields]
    new_record: Optional[SectionFields]

class SectionDelete(CamelModel):
    id: int
    
class SectionUpdate(CamelModel):
    action: str
    old_record: Optional[DataSectionHeader]
    new_record: Optional[DataSectionHeader]
    class Config:
        orm_mode=True

class SectionSave(CamelModel):
    sections: List[dict]
    class Config:
        orm_mode=True

class AdmissionOnboarding(CamelModel):
    form_id: int
    class Config:
        orm_mode=True
