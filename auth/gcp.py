from typing import Optional

from fastapi import APIRouter, Depends, Header, status, HTTPException, Response
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from auth import schemas

import requests
from requests.auth import HTTPBasicAuth

from auth.schemas import User



def create_firebase_token(user:User):
    params = {
        "username": user.username,
        "active": user.active,
        "firstName": user.first_name,
        "lastName": user.last_name,
        "role": user.role
    }
    resp = requests.post("https://firebase.campusbrain.in/create", json=params)
    if resp.ok:
        return resp.json()

    print(resp.json())
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Google firebase failed to create token')