
from enum import Enum
from decouple import config
from pydantic import BaseModel, Field

from fastapi.security.http import HTTPBearer, HTTPAuthorizationCredentials
import os
import json
import time
import requests
import urllib.request
from jose import jwk, jwt
from jose.utils import base64url_decode
from fastapi import HTTPException, status, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

from humps import camelize
from pydantic import BaseModel, Json

auth = HTTPBearer()


class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True
# extend current user info model by `pydantic`.
class CustomClaims(CamelModel):
    user_id: str
    first_name:str
    last_name:str
    role:str
    school_code: str
    picture_url: str


def verify_token(token):
    keys_url = 'https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com'

    keys = requests.get(keys_url).json()
    # get the kid from the headers prior to verification
    try:
        headers = jwt.get_unverified_headers(token)
    except:
        return { "success":False, "detail":'Invalid Token' }
    kid = headers['kid']
    # search for the kid in the downloaded public keys
    if kid not in keys:
        return { "success": False, "detail":'Public key not found in jwks.json' }
    # construct the public key
    try:
        public_key = jwk.construct(keys[kid],algorithm='RS256')
        # public_key = jwk.construct(keys[kid])
    except Exception as e:
        print(e)
        return { "success":False, "detail":'Construct Failed' }
    
    # get the last two sections of the token,
    # message and signature (encoded in base64)
    message, encoded_signature = str(token).rsplit('.', 1)
    # decode the signature
    decoded_signature = base64url_decode(encoded_signature.encode('utf-8'))
    # verify the signature
    if not public_key.verify(message.encode("utf8"), decoded_signature):
        return { "success":False, "detail":'Signature verification failed' }
    # since we passed the verification, we can now safely
    # use the unverified claims
    claims = jwt.get_unverified_claims(token)
    # additionally we can verify the token expiration
    if time.time() > claims['exp']:
        return { "success":False, "detail":'Token is expired' }
    # and the Audience  (use claims['client_id'] if verifying an access token)
    # if claims['aud'] != 'campus-brain-app':
    #     return { "success":False, "detail":'Token was not issued for this audience'}
    # now we can use the claims
    return {"success":True,"claims":claims}
        
async def get_current_firebase_user(http_auth: HTTPAuthorizationCredentials = Depends(auth)):
    token = http_auth.credentials
    resp = verify_token(token)
    if resp["success"]:
        custom_claims = resp["claims"]
        if 'phone_number' in custom_claims:
            return CustomClaims(
                user_id=custom_claims['phone_number'],
                school_code= '',
                first_name= '',
                last_name= '',
                role= 'visitor',
                picture_url= '',
            )
        return CustomClaims(
            user_id=custom_claims["user_id"],
            school_code=custom_claims["schoolCode"],
            first_name=custom_claims["firstName"],
            last_name=custom_claims["lastName"],
            role=custom_claims["role"],
            picture_url=custom_claims["pictureUrl"],
        )
    raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,detail=resp["detail"])
