from fastapi import APIRouter, Depends, status, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
import requests
from sqlalchemy.sql.functions import modifier
from auth import firebase, models, schemas, user
from auth.oauth2 import get_current_active_user, get_current_user
from utils import database
from utils.database import get_db
import uuid
import jinja2

auth_router = APIRouter(tags=['Authentication'], prefix='/user')
GOOGLE_AUTH = "AIzaSyA8addR8PZJJ8iXioIt_msEOJSuvotgQl4"

@auth_router.post('/auth/')
def try_firebase_login(request:schemas.LoginRequest, db: Session = Depends(database.get_db)):
    return user.login(request, db)



@auth_router.post('/token/')
def login_user_swagger(request: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(database.get_db)):
    return user.login(request, db)


@auth_router.post('/otp/')
def verifiy_otp(request:schemas.OTPVerify):
    url = f"https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPhoneNumber?key={GOOGLE_AUTH}"
    data = {
        "code": request.code,
        "sessionInfo": request.sessionInfo,
    }
    resp = requests.post(url,data)
    return resp.json()


@auth_router.post('/token/new/')
def login_user_api(request: schemas.UserLogin, db: Session = Depends(database.get_db)):
    return user.login(request, db)


@auth_router.post('/create/', status_code=status.HTTP_201_CREATED)
def create_user(request: schemas.UserForm, db: Session = Depends(get_db)):
    """
    Create Auth User
    """
    return user.create(request, db)


@auth_router.get('/all/')
def get_all_users(db: Session = Depends(get_db), get_current_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    return db.query(models.User).all()
