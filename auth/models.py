from sqlalchemy import Boolean, Column, Integer, String, true

from utils.database import Base


class User(Base):
    __tablename__ = 'WebsiteUsers'

    id = Column(Integer, primary_key=True, index=True)
    first_name = Column('FirstName',String(156))
    last_name = Column('LastName',String(156))
    username = Column(String(15))
    role = Column(String(15))
    password = Column(String(100))
    active = Column(Boolean(true))
