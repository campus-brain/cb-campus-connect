from fastapi import HTTPException, status
from sqlalchemy.orm import Session

from auth import firebase, gcp, models, schemas, token
from utils.hash import Hash


def create(request: schemas.UserForm, db: Session):
    new_user = models.User(
        username=request.username, first_name=request.first_name, last_name=request.last_name, role=request.role, password=Hash.encrypt(request.password),
        active=True)
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


def login(request: schemas.UserLogin, db: Session):
    user = db.query(models.User).filter(
        models.User.username == request.username).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Invalid Credentials")
    if not Hash.verify(user.password, request.password):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Incorrect password")
    try:
        return gcp.create_firebase_token(user)
    except Exception as e:
        print("Failed to create token")
        print(e)
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail=str(e))
    


def get_users(username: str, db: Session):
    user = db.query(models.User).filter(models.User.username == username).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User with the username {username} is not available")
    return user
