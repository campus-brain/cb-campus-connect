from typing import List, Optional

from pydantic import BaseModel

from auth.firebase import CamelModel


class User(BaseModel):
    username: str
    password: str
    active: bool
    first_name:str
    last_name:str
    role:str

class OTPVerify(BaseModel):
    code: str
    sessionInfo: str
    

class UserForm(CamelModel):
    username: str
    first_name:str
    last_name:str
    role:str
    password: str


class LoginRequest(BaseModel):
    username:str
    password:str

class UserLogin(BaseModel):
    username: str
    password: str


class ShowUser(BaseModel):
    name: str
    username: str
    email: str
    active: bool

    class Config:
        orm_mode = True


class Login(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None


class GoogleLogin(BaseModel):
    access_token: str
