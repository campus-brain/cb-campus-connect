from typing import List, Optional, Union

from humps import camelize
from pydantic import BaseModel, Json, Field
from datetime import datetime, date

class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True


class BookDetails(CamelModel):
    isbn:str
    title:str
    author:str
    description:str
    thumbnail_url:str
    publishers:str
    publish_date:str
    physical_format:str
    class Config:
        orm_mode=True


class Books(CamelModel):
    public_book: BookDetails = Field(alias='PublicBook')
    count: int
    class Config:
        orm_mode=True


class BookStock(CamelModel):
    books:List[Books]
    

class BookRequest(CamelModel):
    isbn:str
    count:int

class BookISBN(CamelModel):
    books:List[BookRequest]


class BookActivityRequest(CamelModel):
    book_id:int
    action:str
    owner:str
    due_date:Optional[date]
    performed_by: str
    class Config:
        orm_mode=True

class BookActivityResponse(CamelModel):
    message: str

class BookActivity(CamelModel):
    id:int
    book_id:int
    action:str
    owner: str
    entry_date:date
    due_date:date
    return_date:Union[date, None]
    performed_by: str
    performed_on: datetime
    fine_alerted: bool
    class Config:
        orm_mode=True

class BookActivities(CamelModel):
    activities: List[BookActivity]
    class Config:
        orm_mode=True

class IndividualBook(CamelModel):
    book_id:int
    owner:Union[str, None]
    class Config:
        orm_mode=True


class BookCollectionWithOwners(CamelModel):
    details: BookDetails
    books: List[IndividualBook]
    class Config:
        orm_mode=True

class SchoolBookJoin(CamelModel):
    SchoolBook: IndividualBook
    PublicBook: BookDetails
    class Config:
        orm_mode=True

class SchoolBookJoinWithActivities(CamelModel):
    book: SchoolBookJoin
    activities: List[BookActivity]
    class Config:
        orm_mode=True

 
class BookActivitiesDetailed(CamelModel):
    PublicBook: BookDetails
    BookActivity: BookActivity
    class Config:
        orm_mode=True

class BookActivitiesWithJoins(CamelModel):
    activities: List[BookActivitiesDetailed]
    class Config:
        orm_mode=True