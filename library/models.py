from sqlalchemy import JSON, Boolean, ForeignKey, Column, Integer, String, Date, DateTime

from utils.database import Base


class PublicBook(Base):
    __tablename__ = 'LibraryBooks'

    isbn = Column('Isbn', String(256), primary_key=True)
    title = Column('Title', String(256))
    author = Column('Author', String(156))
    publishers = Column('Publishers', String(156))
    description = Column('Description', String(156))
    thumbnail_url = Column('ThumbnailUrl', String(200))
    publish_date = Column('PublishDate', String(200))
    physical_format = Column('PhysicalFormat', String(200))


class SchoolBook(Base):
    __tablename__ = 'IndividualLibraryBooks'
    
    book_id = Column('id', Integer, primary_key=True, autoincrement=True)
    isbn = Column('Isbn', String(256), ForeignKey("LibraryBooks.Isbn"))
    owner = Column('Owner', String(156), nullable=True)
    
class BookActivity(Base):
    __tablename__ = "BookActivity"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    book_id = Column('BookId', Integer, ForeignKey("IndividualLibraryBooks.id"))
    action = Column('Action', String(256))
    owner = Column('Owner', String(156), nullable=True)
    entry_date = Column('EntryDate', Date, nullable=True)
    due_date = Column('DueDate', Date, nullable=True)
    return_date = Column('ReturnDate', Date, nullable=True)
    performed_by = Column('PerformedBy', String(256))
    performed_on = Column('PerformedOn', DateTime)
    fine_alerted = Column('FineAlerted', Boolean, default=False)
