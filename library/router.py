from typing import Optional

from fastapi import APIRouter, Depends, Header, status, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from library import schemas, models, books
from utils import database
from utils.database import get_db
import requests
from datetime import datetime

library_router = APIRouter(tags=['Library'], prefix='/books')


@library_router.get("/",response_model=schemas.BookStock)
def get_all_books(isbn:Optional[str] = None, title:Optional[str] = None, author:Optional[str] = None ,page:Optional[int]=1, db:Session = Depends(database.get_db)):
    """
    Get all books
    """
    return books.get_all_books(isbn, title, author, page, db)

@library_router.post("/isbn/")
def populate_book_by_isbn(request:schemas.BookISBN, db:Session = Depends(database.get_db)):
    """
    Get populate book by isbn
    """
    return books.create_book(request,db)

@library_router.get("/{id}/", response_model=schemas.SchoolBookJoinWithActivities)
def get_school_book(id:int, db:Session = Depends(database.get_db)):
    """
    Get school book
    """
    return books.get_school_book(id, db)
    
@library_router.get("/isbn/{isbn}/", response_model=schemas.BookCollectionWithOwners)
def get_books_grouped_by_isbn(isbn:str, db:Session = Depends(database.get_db)):
    """
    Get school book
    """
    return books.get_books_grouped_by_isbn(isbn, db)

@library_router.post("/action/",response_model=schemas.BookActivityResponse)
def create_book_activity(request:schemas.BookActivityRequest, db:Session = Depends(database.get_db)):
    """
    Get school book
    """
    return books.create_book_activity(request, db)

@library_router.get("/action/history/", response_model=schemas.BookActivitiesWithJoins)
def get_book_activity(page:int=1,  book_id:Optional[int]=None, owner:Optional[str]=None, start_date:Optional[datetime]=None,end_date:Optional[datetime]=None,db:Session = Depends(database.get_db)):
    """
    Get school book
    """
    return books.get_book_activities(book_id,owner, start_date, end_date, page, db)