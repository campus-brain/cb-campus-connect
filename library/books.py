from typing import Optional
from sqlalchemy import func, select, column
from fastapi import APIRouter, Depends, Header, status, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from library import schemas, models
from utils import database
from utils.database import get_db
import requests
from sqlalchemy.exc import IntegrityError
from datetime import datetime, timezone, date
from fastapi.encoders import jsonable_encoder

GOOGLE_AUTH = "AIzaSyBLbJ7b9po4c7O4fhGA9fbGYMl3Shzsnh4"

def getList(dict):
    return [*dict]

def get_all_books(isbn:str,title:str,author:str,page:int, db:Session):
    """Get all Books

    Args:
        isbn (str): ISBN of the book
        title (str): Title of the book
        author (str): Author of the book
        page (int): Paginator
        db (Session): DB

    Returns:
        [type]: [description]
    """
    page_size = 25
    result = db.query((models.PublicBook), (func.count(models.SchoolBook.book_id)).label('count')) \
        .outerjoin(models.SchoolBook) \
        .group_by(models.PublicBook) \
        .filter(not isbn or (isbn!="" and models.PublicBook.isbn == isbn)) \
        .filter(not title or (title!="" and func.lower(models.PublicBook.title).contains(title))) \
        .order_by(models.PublicBook.title.desc()) \
        .filter(not author or (author!="" and func.lower(models.PublicBook.author).contains(author))) \
        .offset((page-1)*page_size) \
        .limit(page_size).all()
    return {"books": jsonable_encoder(result)}


def get_books_grouped_by_isbn(isbn:str, db:Session):
    result = db.query((models.SchoolBook)) \
        .filter(models.SchoolBook.isbn == isbn).all()
    
    book_detail = db.query(models.PublicBook)\
        .filter(models.PublicBook.isbn == isbn).first()
    return {"books":jsonable_encoder(result), "details":jsonable_encoder(book_detail)}

def get_school_book(id:int, db:Session):
    result_copy = db.query(models.SchoolBook,models.PublicBook).outerjoin(models.PublicBook).filter(models.SchoolBook.book_id == id)
    result = result_copy.first()
    activities = db.query(models.BookActivity).filter(models.BookActivity.book_id == id).order_by(models.BookActivity.id.desc()).all()
    return {"book":result, "activities":jsonable_encoder(activities)}

def get_book_activities(book_id,owner, start_date, end_date, page, db):
    page_size = 25
    result = db.query(models.BookActivity,models.PublicBook) \
        .outerjoin(models.SchoolBook,models.SchoolBook.book_id == models.BookActivity.book_id ) \
        .outerjoin(models.PublicBook,models.PublicBook.isbn == models.SchoolBook.isbn ) \
        .filter(not book_id or (book_id == -1 or models.BookActivity.book_id == book_id)) \
        .filter(not owner or (owner!="" and models.BookActivity.owner == owner)) \
        .order_by(models.BookActivity.id.desc()) \
        .offset((page-1)*page_size) \
        .limit(page_size).all()
    print(result)
    if result:
        return {"activities": jsonable_encoder(result)}
    return {"activities": []}

def create_book_activity(request:schemas.BookActivity, db:Session):
    result_copy = db.query(models.SchoolBook).filter(models.SchoolBook.book_id == request.book_id)
    result = result_copy.first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Book Not Found")
    # check if real owner


    activity = {
        "book_id": request.book_id,
        "action": request.action,
        "owner": request.owner,
        "performed_by": request.performed_by,
        "performed_on": datetime.now(tz=timezone.utc),
        "fine_alerted": False
    }
    
    activities = []
    message = ""
    if request.action == 'assign' and (not result.owner or result.owner != "") :
        result_copy.update({"owner": request.owner})
        activity['entry_date'] = date.today()
        activity['due_date'] = request.due_date
        activities.append(activity)
        message = f"Successfully Assigned to {request.owner}"

    elif request.action == 'return' and request.owner == result.owner:
        
        activity_result_copy = db.query(models.BookActivity) \
        .filter(models.BookActivity.action == 'assign' and models.BookActivity.owner == request.owner and models.BookActivity.book_id == request.book_id)
        activity_result = activity_result_copy.order_by(models.BookActivity.id.desc()).first()
        
        activity_result_copy.update({
            'action': 'return',
            'return_date': date.today()
        })
        
        result_copy.update({"owner": None})
        db.commit()
        db.refresh(result)
        db.refresh(activity_result)
        message = f"Book Returned by {request.owner}"

    elif request.action == 'assign' and request.owner != result.owner :

        activity_result_copy = db.query(models.BookActivity) \
        .filter(models.BookActivity.action == 'assign' and models.BookActivity.owner == request.owner and models.BookActivity.book_id == request.book_id)
        activity_result = activity_result_copy.order_by(models.BookActivity.id.desc()).first()
        
        result_copy.update({"owner": request.owner})
        activity_result_copy.update({
            'action': 'return',
            'return_date': date.today()
        })
        
        db.commit()
        db.refresh(result)
        db.refresh(activity_result)
        message = f"Book Returned by {request.owner} \n"
        _activity = activity.copy()
        _activity["owner"] = request.owner
        _activity["action"] = "assign"
        _activity['entry_date'] = date.today()
        _activity['due_date'] = request.due_date

        activities.append(_activity)
        message += f"Successfully Assigned to {request.owner}"
    db.commit()
    db.refresh(result)
    db.bulk_insert_mappings(models.BookActivity, activities)
    db.commit()

    return {"message": message}
def create_book(request:schemas.BookISBN,db:Session):
    try:
        books = []
        individual_books = []
        ignored_isbns = []
        duplicate_isbns = []
        all_requested_isbns = [k.isbn for k in request.books]
        
        db_books = db.query(models.PublicBook).filter(models.PublicBook.isbn.in_(all_requested_isbns)).all()
        for book in db_books:
            duplicate_isbns.append(book.isbn)
        for book in request.books:
            try:
                if book.isbn not in duplicate_isbns:
                    book_resp = requests.get(f"https://books.googleapis.com/books/v1/volumes?q=isbn%3A{book.isbn}&key={GOOGLE_AUTH}")
                    if not book_resp.ok:
                        ignored_isbns.append(book.isbn)
                        continue
                        
                    records = book_resp.json()["items"][0]
                    overview = records["volumeInfo"]
                    new_book = {
                        "isbn":book.isbn,
                        "title":overview["title"],
                        "thumbnail_url":overview["imageLinks"]["thumbnail"] if "imageLinks" in overview else "",
                        "physical_format":overview["printType"],
                        "description":overview["description"],
                        "publish_date":overview["publishedDate"],
                        "publishers": overview["publisher"] if "publisher" in overview else "",
                        "author": ", ".join([item for item in overview["authors"]]) if "authors" in overview else ""
                    }
                    books.append(new_book)

                for _ in range(0,book.count):
                    new_individual_book = {
                        "isbn":book.isbn,
                        "owner":""
                    }
                    individual_books.append(new_individual_book)
            except:
                ignored_isbns.append(book.isbn)
        db.bulk_insert_mappings(models.PublicBook, books)
        db.commit()
        db.bulk_insert_mappings(models.SchoolBook, individual_books)
        db.commit()
        return {"books":ignored_isbns}
    except IntegrityError as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail="Duplicate Book")