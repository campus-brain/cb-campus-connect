from fastapi import FastAPI
from fastapi.params import Depends
from fastapi.responses import RedirectResponse
from mangum import Mangum
from fastapi.middleware.cors import CORSMiddleware

from auth import firebase, models
from auth.router import auth_router
from admin.router import admin_router
from admissions.router import admin_admissions_router, public_admissions_router, unauthorized_admissions_router
from employee.router import employee_router
from library.router import library_router
from payments.router import payments_router

from academic.router import academic_router
from utils.database import get_engine



models.Base.metadata.create_all(bind=get_engine())

app = FastAPI(
        title="Campus Brain Campus Connect",
        version="1.0.0"
)

# origins = [
#     "http://localhost:4200",
#     "http://localhost:8000",
#     "https://dummy.campusbain.in",
#     "https://middleware.campusbain.in",
# ]

origins = ["*"]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



app.include_router(auth_router)
app.include_router(admin_router,dependencies=[Depends(firebase.get_current_firebase_user)])
app.include_router(employee_router,dependencies=[Depends(firebase.get_current_firebase_user)])
app.include_router(library_router,dependencies=[Depends(firebase.get_current_firebase_user)])
# app.include_router(library_router)
app.include_router(admin_admissions_router,dependencies=[Depends(firebase.get_current_firebase_user)])
app.include_router(unauthorized_admissions_router)
app.include_router(public_admissions_router,dependencies=[Depends(firebase.get_current_firebase_user)])
app.include_router(payments_router,dependencies=[Depends(firebase.get_current_firebase_user)])
app.include_router(academic_router,dependencies=[Depends(firebase.get_current_firebase_user)])

@app.get("/", response_class=RedirectResponse)
async def redirect_docs():
    return RedirectResponse(url='/docs')


@app.post("/token", response_class=RedirectResponse)
async def redirect_token():
    return RedirectResponse(url='/user/token')


handler = Mangum(app, lifespan="off")

