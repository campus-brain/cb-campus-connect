from sqlalchemy import JSON, Boolean, ForeignKey, Column, Integer, String, Date, DateTime

from utils.database import Base

    
class UserTransactions(Base):
    __tablename__ = "UserTransactions"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    user_id = Column('UserId', String(56))
    category = Column('Category', String(256))
    category_id = Column('CategoryID', String(156), nullable=True)
    amount = Column('Amount', Integer, nullable=True)
    order_id = Column('OrderID', String(200), nullable=True)
    payment_id = Column('TxnID', String(200), nullable=True)
    is_successful = Column('IsSuccessful', Boolean, default=False)
    transaction_date = Column('TransactionDate', DateTime)
    
