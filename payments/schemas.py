from pydantic import BaseModel


class RazorpayOrder(BaseModel):
    amount: float

class RazorpayOrderResponse(BaseModel):
    razorpay_order_id: str
    razorpay_payment_id: str
    razorpay_signature: str

class Order(BaseModel):
    orderId: str
    amount: float

class UserDetail(BaseModel):
    user_id: str
    category: str
    meta: dict

class RazorPayUserDetail(BaseModel):    
    user_id: str
    category: str
    meta: dict

class PaytmTransactionResponse(BaseModel):
    BANKNAME: str
    BANKTXNID: str
    CHECKSUMHASH: str
    CURRENCY: str
    GATEWAYNAME: str
    MID: str
    ORDERID: str
    PAYMENTMODE: str
    RESPCODE: str
    RESPMSG: str
    STATUS: str
    TXNAMOUNT: str
    TXNDATE: str
    TXNID: str

class PaymentVerification(BaseModel):
    user: UserDetail
    payment: PaytmTransactionResponse

class RazorpayPaymentVerification(BaseModel):
    user: RazorPayUserDetail
    payment: RazorpayOrderResponse

