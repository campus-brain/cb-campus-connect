from fastapi import APIRouter, Depends, status
from sqlalchemy.orm import Session

from auth import schemas as AuthSchemas
from utils.database import get_db
from payments import repo, schemas

payments_router = APIRouter(tags=['Payments'], prefix='/payments/orders')


@payments_router.post('/create/razorpay/')
def create_razorpay_order(request:schemas.RazorpayOrder):
    return repo.create_razorpay_order(request)
    
@payments_router.post('/verify/razorpay/')
def verify_razorpay_payment(request:schemas.RazorpayPaymentVerification, db: Session = Depends(get_db)):
    return repo.verify_razorpay_payment(request,db)

@payments_router.post('/create/')
def create_order(request:schemas.Order):
    return repo.create_order(request)
    
@payments_router.post('/verify/')
def verify_order(request:schemas.PaymentVerification, db: Session = Depends(get_db)):
    return repo.verify_payment(request,db)    

