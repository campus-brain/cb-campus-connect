import json
import math

import requests
import razorpay
from fastapi import status , HTTPException
from fastapi.responses import JSONResponse
from datetime import datetime, timezone
from payments import schemas, models
from sqlalchemy.orm import Session
from admissions.updates import get_application_overview, update_application_overview_status, update_application_detail_status

PAYTM_MERCH_ID = "QGSBPW55549735521417"
PAYTM_MERCH_KEY = "K1HJ5Zdj83e6Y_wv"

RAZORPAY_API_KEY = "rzp_test_TKOaH5A7aLp412"
RAZORPAY_API_SECRET = "KBNhlGgDeOyVBxwWeMvgCWNt"

razorpay_client = razorpay.Client(auth=(RAZORPAY_API_KEY, RAZORPAY_API_SECRET))



def create_razorpay_order(data:schemas.RazorpayOrder):
    data = {
        "amount": data.amount,
        "currency": "INR"
    }
    return razorpay_client.order.create(data=data)


def update_records(user_details,payment_data,order_details, db:Session):
    application_id = None
    
    if user_details.category == 'admissions':
        meta = user_details.meta
        form_id = meta['formId']
        submission_id = meta['submissionId']
        matched_form = get_application_overview(form_id, submission_id, db)
        application_id = matched_form.id
    if not application_id:
        return None
    new_transaction = models.UserTransactions(
        user_id = user_details.user_id,
        category = user_details.category,
        category_id = application_id,
        amount = float(order_details['amount']/100),
        order_id = payment_data['razorpay_order_id'],
        payment_id = payment_data['razorpay_payment_id'],
        is_successful = True if order_details['status'] == 'captured' else False,
        transaction_date = datetime.now(tz=timezone.utc)
    )
    db.add(new_transaction)
    db.commit()
    update_application_overview_status(application_id, 'paid', db)
    update_application_detail_status(application_id, 'paid', db)
def verify_razorpay_payment(user_payment:schemas.RazorpayPaymentVerification,db:Session):
    
    try:
        payment_data = user_payment.payment
        user_details = user_payment.user
        params_dict = {
            "razorpay_order_id": payment_data.razorpay_order_id,
            "razorpay_payment_id": payment_data.razorpay_payment_id,
            "razorpay_signature": payment_data.razorpay_signature,
        }
        razorpay_client.utility.verify_payment_signature(params_dict)
        order_details = razorpay_client.payment.fetch(params_dict['razorpay_payment_id'])
        _payment_data = {
            "razorpay_order_id": payment_data.razorpay_order_id,
            "razorpay_payment_id": payment_data.razorpay_payment_id,
            "razorpay_signature": payment_data.razorpay_signature
        }
        update_records(user_details, _payment_data, order_details, db)
      
    except Exception as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail="Invalid Signature")
   
     
def create_order(data:schemas.Order):
    try:
        paytm_params = dict()
        paytm_params["body"] = {
            "requestType": "Payment",
            "mid": PAYTM_MERCH_ID,
            "websiteName": "WEBSTAGING",
            "orderId": data.orderId,
            "txnAmount": {
                "value": "{:.2f}".format((data.amount)),
                "currency": "INR",
            },
            "userInfo": {
                "custId": "CUST_001",
            },
            # "enablePaymentMode": [{"mode": "UPI"}, {"mode": "BALANCE"}, {"mode": "PPBL"}, {"mode": "UPI"}, {"mode": "CREDIT_CARD"}, {"mode": "DEBIT_CARD"}, {"mode": "INTERNET_BANKING"}]
        }
        checksum_params = {
            "BODY": json.dumps(paytm_params["body"]),
            "MKEY": PAYTM_MERCH_KEY
        }
        resp = requests.post("https://payments.campusbrain.in/generate", json=checksum_params)

        if not resp.ok:
            return JSONResponse(content={"message": "Error producing checksum"}, status_code=status.HTTP_400_BAD_REQUEST)
        checksum = resp.json()['checksum']
        paytm_params["head"] = {
            "signature": checksum
        }
        post_data = json.dumps(paytm_params)

        # for Staging
        url = f"https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid={PAYTM_MERCH_ID}&orderId={data.orderId}"

        # for Production
        # url = "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=YOUR_MID_HERE&orderId=ORDERID_98765"
        response = requests.post(url, data=post_data, headers={"Content-type": "application/json"})
        resp_json = response.json()
        return JSONResponse(content=response.json(), status_code=status.HTTP_400_BAD_REQUEST if resp_json["body"]["resultInfo"]["resultStatus"] == 'F' else status.HTTP_200_OK)
    except KeyError:
        return JSONResponse(content={"message": "missing fields"}, status_code=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        print(type(e).__name__)
        print(e)
        return JSONResponse(content={"message": f"Something went wrong {e}"}, status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

def verify_payment(user_payment:schemas.PaymentVerification,db:Session):
    checksum = user_payment.payment.CHECKSUMHASH

    user_details:schemas.UserDetail = user_payment.user
    paytm_success_response_duplicate:schemas.PaytmTransactionResponse = user_payment.payment
    paytm_success_response = user_payment.payment.dict(exclude={'CHECKSUMHASH'})
    checksum_params = {
        "BODY": paytm_success_response,
        "CHECKSUM": checksum,
        "MKEY": PAYTM_MERCH_KEY
    }
    verification_url = "https://payments.campusbrain.in/verify"
    resp = requests.post(verification_url, json=checksum_params)

    if not resp.ok:
        return JSONResponse(content={"message": "Error producing checksum"}, status_code=status.HTTP_400_BAD_REQUEST)
    resp_json = resp.json()
    application_id = None
    if user_details.category == 'admissions':
        meta = user_details.meta
        form_id = meta['formId']
        submission_id = meta['submissionId']
        matched_form = get_application_overview(form_id, submission_id, db)
        application_id = matched_form.id
    new_transaction = models.UserTransactions(
        user_id = user_details.user_id,
        category = user_details.category,
        category_id = application_id,
        amount = float(paytm_success_response_duplicate.TXNAMOUNT),
        order_id = paytm_success_response_duplicate.ORDERID,
        payment_id = paytm_success_response_duplicate.TXNID,
        bank_txn_id = paytm_success_response_duplicate.BANKTXNID,
        is_successful = True if paytm_success_response_duplicate.STATUS == 'TXN_SUCCESS' else False,
        transaction_date = datetime.now(tz=timezone.utc)
    )
    db.add(new_transaction)
    db.commit()
    if paytm_success_response_duplicate.STATUS == 'TXN_SUCCESS':
        print(application_id)
        update_application_overview_status(application_id, 'paid', db)
        update_application_detail_status(application_id, 'paid', db)
    return JSONResponse(content=resp_json, status_code=status.HTTP_200_OK if resp_json["success"] else status.HTTP_400_BAD_REQUEST)