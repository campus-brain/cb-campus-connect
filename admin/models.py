from sqlalchemy import Boolean, Column, Integer, String, true
from sqlalchemy.sql.schema import ForeignKeyConstraint, PrimaryKeyConstraint
from sqlalchemy.sql.sqltypes import Date

from utils.database import Base


class Student(Base):
    __tablename__ = 'Student'

    student_id = Column('StudentID', String(15), primary_key = True)
    first_name = Column('FirstName', String(50))
    last_name = Column('LastName', String(50))
    mobile_number = Column('MobileNumber', String(20))
    guardian_id = Column('GuardianID', String(20))
    gender = Column("Gender", String(15))
    date_of_birth = Column("DateOfBirth", Date)

class Guardian(Base):
    __tablename__ = 'Guardian'

    guardian_id = Column('GuardianID', String(15), primary_key = True)
    father_first_name = Column('FatherFirstName', String(50))
    father_last_name = Column('FatherLastName', String(50))
    father_mobile_number = Column('FatherMobileNumber', String(20))
    mother_first_name = Column('MotherFirstName', String(50))
    mother_last_name = Column('MotherLastName', String(50))
    mother_mobile_number = Column('MotherMobileNumber', String(20))
    primary_mobile_number = Column('PrimaryMobileNumber', String(20))


