from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from utils import schemas, models
from utils import database, data_fields
from admin.models import Student, Guardian
#from admissions.views import get_submission_record
from sqlalchemy import text
import json
import time
import uuid

with open('admin/admission_onboarding_mapper.json') as f:
    admission_onboarding_mapper = json.load(f)


def get_student_fields(db: Session):
    return data_fields.get_fields('student', db)


def update_student_fields(request: schemas.DataFieldUpdate, db: Session):
    table_name = request.section.id
    if request.section.id not in ["Student", "ParentsGuardian"]:
        table_name = "StudentAdditionalFields"
    return data_fields.update_fields('student', table_name, request, db)


def update_student_section(request: schemas.SectionUpdate, db: Session):
    table_name = None
    is_additional_table = False
    if request.old_record:
        table_name = request.old_record.id
    if table_name and (table_name not in ["Student", "ParentsGuardian"]):
        table_name = "StudentAdditionalFields"
        is_additional_table = True
    return data_fields.update_sections('student', table_name, request, is_additional_table, db)


def get_accepted_records(table_name_string: str, db: Session):
    selected_table = database.get_table(table_name_string)
    query = selected_table.select().where(selected_table.c.Stage == "accepted")

    rows = db.execute(query)
    return rows


def get_guardian_records(db: Session):
    return db.query(Guardian.guardian_id, Guardian.primary_mobile_number).all()


def search(dict, mobile_number):
    for record in dict:
        if record['PrimaryMobileNumber'] == mobile_number:
            return record["GuardianID"]
    return False


def bulk_insert_students(records, db:Session):
    objects = []
    
    for record in records:
        new_record = Student(
            student_id=record.get("StudentID", None), first_name=record.get("FirstName", None), last_name=record.get("LastName", None),
            gender = record.get("Gender", None), guardian_id = record.get("GuardianID", None), date_of_birth = record.get("DateOfBirth", None),
            active=True)
        objects.append(new_record)
    #db.bulk_insert_mappings(Student, records)
    result = db.add_all(objects)
    return result


def bulk_insert_guardians(records, db):
    #db.bulk_insert_mappings(Guardian, records)
    objects = []
    
    for record in records:
        new_record = Guardian(
            guardian_id = record.get("GuardianID", None), father_first_name=record.get("FatherFirstName", None), father_last_name=record.get("FatherLastName", None),
            father_mobile_number = record.get("FatherMobileNumber", None), mother_first_name=record.get("MotherFirstName", None), mother_last_name=record.get("MotherLastName", None),
            mother_mobile_number = record.get("MotherMobileNumber", None), primary_mobile_number = record.get("PrimaryMobileNumber", None),
            active=True)
        objects.append(new_record)
    #db.bulk_insert_mappings(Student, records)
    result = db.add_all(objects)
    return result


def bulk_admission_onboarding(form_id, db: Session):
    start_time = time.time()
    table_name_string = "admission"+str(form_id)

    # get records and columns from admission table
    rows = get_accepted_records(table_name_string, db)
    admission_columns = database.get_columns(table_name_string)

    print("--- %s seconds ---" % (time.time() - start_time))

    # Records and columns mapper
    admission_list = list(
        map(lambda row: dict(zip(admission_columns, row)), rows))

    print("--- %s seconds ---" % (time.time() - start_time))

    guardian_records = get_guardian_records(db)
    guardian_records = list(
        map(lambda row: dict(zip(["GuardianID", "PrimaryMobileNumber"], row)), guardian_records))
    print("--- %s seconds ---" % (time.time() - start_time))

    # Lookup guardian ID or create and assign a guardian ID
    for record in admission_list:
        search_result = search(
            guardian_records, record["ParentDetailsFatherMobileNumber"])
        if search_result:
            record["GuardianID"] = search_result
            record["RecordExist"] = True
        else:
            record["GuardianID"] = str(uuid.uuid4())[:15]
            record["RecordExist"] = False
        if record.get("ParentDetailsPrimaryMobileNumber", None) == None:
            record["ParentDetailsPrimaryMobileNumber"] = record["ParentDetailsFatherMobileNumber"]
        record["StudentID"] = str(uuid.uuid4())[:15]

    result_set = list()
    parsed_keys = []

    students = list(map(lambda row: dict((value, row.get(key, None)) for (
        key, value) in admission_onboarding_mapper.get('Student').items()), admission_list))
    parsed_keys = parsed_keys + \
        list(admission_onboarding_mapper.get('Student').keys())

    guardians = list(map(lambda row: dict((value, row.get(key, None)) for (
        key, value) in admission_onboarding_mapper.get('Guardian').items()) if row["RecordExist"] == False else None, admission_list))
    parsed_keys = parsed_keys + \
        list(admission_onboarding_mapper.get('Guardian').keys())

    # get mappings for student additional fields and update the unparsed keys
    student_additional_fields_dict = admission_onboarding_mapper.get(
        'StudentAdditionalFields')
    parsed_keys_dict = dict((key, None) for key in (
        set(admission_columns) - set(parsed_keys)))
    student_additional_fields_dict.update(parsed_keys_dict)
    student_additional_fields = list(map(lambda row: dict((value, row.get(key, None)) for (
        key, value) in student_additional_fields_dict.items()), admission_list))

    guardians = [record for record in guardians if record is not None]
    print(students)
    print(guardians)

    bulk_insert_students(students, db)
    bulk_insert_guardians(guardians, db)
    print("Bulk insertion successful")
    return guardians
    selected_table = database.get_table('StudentAdditionalFields')
    query = selected_table.insert().values(student_additional_fields)
    rows = db.execute(query)
    print(rows)

    return guardians
