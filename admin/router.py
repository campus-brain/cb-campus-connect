from typing import Optional

from fastapi import APIRouter, Depends, Header, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from admin import students
from utils import schemas,models
from utils import database
from utils.database import get_db

admin_router = APIRouter(tags=['Admin - Students'], prefix='/admin')

@admin_router.get("/student/fields/",response_model=schemas.DataField)
def get_student_fields(db:Session = Depends(database.get_db)):
    """
    Get Student fields of the school
    """
    return students.get_student_fields(db)

@admin_router.post("/student/sections/update/",response_model=schemas.DataField)
def update_student_section(request:schemas.SectionUpdate,db:Session = Depends(database.get_db)):
    """
    Update Student section of the school
    """
    return students.update_student_section(request,db)

@admin_router.post("/student/fields/update/",response_model=schemas.DataField)
def update_student_fields(request:schemas.DataFieldUpdate,db:Session = Depends(database.get_db)):
    """
    Update Student fields of the school
    """
    return students.update_student_fields(request,db)

@admin_router.post("/student/admission-onboarding/{form_id}")
def bulk_admission_onboarding(form_id:int, db:Session = Depends(database.get_db)):
    """
    Bulk admission form to student and guardian creation
    """
    return students.bulk_admission_onboarding(form_id,db)
