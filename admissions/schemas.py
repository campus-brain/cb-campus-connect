from datetime import date, datetime
from typing import List, Optional

from humps import camelize
from pydantic import BaseModel


class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class ApplicationInitiation(CamelModel):
    login_id: str
    visitor_id: str

class AdmissionFormRequestSchema(CamelModel):
    name: str
    start_date: datetime
    end_date: datetime
    max_applications: int
    fees: int
    created_by: str
    template: str
    login_required: bool
    description: str
    class Config:
        orm_mode=True

class AdmissionFormPatchRequestSchema(CamelModel):
    id: int
    name: str
    start_date: datetime
    end_date: datetime
    max_applications: int
    fees: int
    created_by: str
    login_required: bool
    description:str
    
    class Config:
        orm_mode=True

class AdmissionFormSchema(CamelModel):
    id: int
    name: str
    start_date: datetime
    end_date: datetime
    max_applications: int
    fees: int
    created_by: str
    created_at: date
    description: str
    login_required: bool
    configuration:dict
    public_configuration:dict

    class Config:
        orm_mode=True

class AdmissionFormList(CamelModel):
    form_list: List[AdmissionFormSchema]
    class Config:
        orm_mode=True

class AdmissionApplicationDetailSchema(CamelModel):
    id: int
    application_id: int
    stage: str
    visitor_id: str
    form_id: int
    last_modified: datetime
    class Config:
        orm_mode=True

class CustomLinkMeta(CamelModel):
    title: str
    link: str
    class Config:
        orm_mode=True

class CustomLinks(CamelModel):
    links: List[CustomLinkMeta]
    class Config:
        orm_mode=True

class NewPublicApplicationData(CamelModel):
    pdf: dict
    record: AdmissionApplicationDetailSchema

class AdmissionFormLimitedData(CamelModel):
    id: int
    name: str
    login_required:bool
    public_configuration:dict
    fees: str
    class Config:
        orm_mode=True
    
class AdmissionApplicationSchema(CamelModel):
    AdmissionApplications:AdmissionApplicationDetailSchema 
    AdmissionForms: AdmissionFormLimitedData
    class Config:
        orm_mode=True

class AdmissionApplicationList(CamelModel):
    applications: List[AdmissionApplicationSchema]
    class Config:
        orm_mode=True

class UserTransaction(CamelModel):
    amount: float
    user_id: str
    payment_id: str
    transaction_date: datetime
    id: int
    category: str
    category_id: int
    order_id: str
    is_successful: bool
    class Config:
        orm_mode=True

class AdmissionHtml(CamelModel):
    html: str


class StageMoveBulkRequest(CamelModel):
    stage: str
    ids: List[int]

class PdfIDs(CamelModel):
    ids: List[int]

class ApplicationPayments(CamelModel):
    AdmissionApplications: AdmissionApplicationDetailSchema 
    UserTransactions: UserTransaction
    class Config:
        orm_mode=True

class ApplicationPaymentsList(CamelModel):
    payments: List[ApplicationPayments]
    class Config:
        orm_mode=True