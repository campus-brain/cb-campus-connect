from sqlalchemy import JSON, Boolean, Column, Integer, String
from sqlalchemy.sql.sqltypes import Date, DateTime

from utils.database import Base


class AdmissionForms(Base):
    __tablename__ = 'AdmissionForms'

    id = Column('Id',Integer, primary_key=True, index=True)
    
    name = Column('Name',String(256))
    description = Column('Description',String(1056), default='', nullable=True)
    start_date = Column('StartDate', DateTime)
    end_date = Column('EndDate', DateTime)
    max_applications = Column('MaxApplications',Integer)
    fees = Column('Fees',Integer)
    created_by = Column('CreatedBy',String(56))
    created_at = Column('CreatedAt', DateTime)
    login_required = Column('LoginRequired', Boolean, default={})
    public_configuration = Column('PublicConfiguration', JSON, default={})
    configuration = Column('Configuration', JSON, default={})

class AdmissionApplications(Base):
    __tablename__ = 'Applications'
    id = Column('Id',Integer, primary_key=True, index=True)
    visitor_id = Column('VisitorID',String(56))
    login_id = Column('LoginID',String(56))
    form_id = Column('FormID',Integer)
    application_id = Column('ApplicationID',Integer)
    stage = Column('Stage',String(56))
    last_modified = Column('LastModified', DateTime)