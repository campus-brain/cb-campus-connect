from os import stat
from typing import List, Optional

from fastapi import APIRouter, Depends, Header, HTTPException, status
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import false

from admissions import updates
from admissions import schemas as admission_schemas
from auth import firebase
from payments.schemas import RazorPayUserDetail
from admissions import views
from utils import database, models, schemas
from utils.database import get_db
from functools import wraps

admin_admissions_router = APIRouter(tags=['Admin - Admissions'], prefix='/admin/admissions')
public_admissions_router = APIRouter(tags=['Admin - Admissions'], prefix='/admissions')
unauthorized_admissions_router = APIRouter(tags=['Admin - Admissions'], prefix='/admissions')

def auth_required(roles = []):
    def actual_decorator(func):
        @wraps(func)
        async def wrapper(website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user), *args,**kwargs):
            if len(roles)>0 and website_user.role and website_user.role not in roles:
                raise HTTPException(status_code=status.HTTP_403_FORBIDDEN) 
            return func(*args, **kwargs)
        return wrapper
    return actual_decorator


def check_if_admin_or_owner(website_user: firebase.CustomClaims, form_id, submission_id, db:Session):
    application = updates.get_application_overview(form_id, submission_id, db)
    resp = website_user.role == 'staff' or website_user.user_id == application.visitor_id
    if not resp:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN) 

@admin_admissions_router.post("/forms/submissions/", response_model=admission_schemas.AdmissionApplicationList)
def get_all_submissions(request: admission_schemas.ApplicationInitiation,db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    if website_user.user_id != request.visitor_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN) 
    k = views.get_all_submissions(request.visitor_id, db)
    print(k)
    return k

@admin_admissions_router.get("/forms/{form_id}/submissions/")
def get_submissions(form_id:int,db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    return views.get_submissions(form_id, db)

@unauthorized_admissions_router.post("/forms/{form_id}/submissions/public/")
def get_public_submissions(form_id:int, request:dict, db:Session = Depends(database.get_db)):
    
    return views.get_public_submissions(form_id,request, db)

@admin_admissions_router.post("/forms/{form_id}/submissions/")
def create_application_form(form_id:int,request: admission_schemas.ApplicationInitiation,db:Session = Depends(database.get_db)):
    return views.create_application_form(form_id, request.visitor_id,request.login_id ,db)

@unauthorized_admissions_router.post("/forms/{form_id}/submissions/new/", response_model=admission_schemas.NewPublicApplicationData)
def create_public_application_form(form_id:int,request: dict,db:Session = Depends(database.get_db)):
    return views.create_public_application_form(form_id,request ,db)

@unauthorized_admissions_router.get("/custom-links/", response_model=admission_schemas.CustomLinks)
def get_admission_custom_links(db:Session = Depends(database.get_db)):
    return views.get_admission_custom_links(db)

@admin_admissions_router.patch("/custom-links/")
@auth_required(['staff'])
def update_admission_custom_links(request: admission_schemas.CustomLinks, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    return views.update_admission_custom_links(request, db)

@admin_admissions_router.get("/forms/{form_id}/submissions/{submission_id}/")
def get_submission_by_id(form_id:int,submission_id:int, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    check_if_admin_or_owner(website_user,form_id, submission_id, db)
    return views.get_submission_by_id(form_id, submission_id, db)

@admin_admissions_router.get("/forms/{form_id}/submissions/{submission_id}/generate-pdf/")
def get_submission_by_id(form_id:int,submission_id:int, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    check_if_admin_or_owner(website_user,form_id, submission_id, db)
    file_name = "patrick/admission" + str(form_id) +".html"
    html =  views.read_s3_file("admission-forms", file_name)
    return views.generate_pdfs(form_id, html,[submission_id],db)

@public_admissions_router.get("/forms/{form_id}/submissions/{submission_id}/lock/")
def lock_submission_by_id(form_id:int,submission_id:int, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    check_if_admin_or_owner(website_user,form_id, submission_id, db)
    return views.lock_submission_by_id(form_id, submission_id, db)

@admin_admissions_router.post("/forms/{form_id}/submissions/{submission_id}/pay/")
@auth_required(['staff'])
def record_cash_transaction_by_submission_id(form_id:int,submission_id:int,request:RazorPayUserDetail, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    return views.record_cash_transaction_by_submission_id(form_id, submission_id,request, db)

@unauthorized_admissions_router.get("/about-us/")
def get_admission_about_us(db:Session = Depends(database.get_db)):
    return views.get_admission_content('admission_about_us', db)
    
@admin_admissions_router.patch("/about-us/")
@auth_required(['staff'])
def update_admission_about_us(request:dict, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    return views.update_admission_content('admission_about_us', request['html'], db)
    
@unauthorized_admissions_router.get("/instructions/")
def get_admission_instructions(db:Session = Depends(database.get_db)):
    return views.get_admission_content('admission_instructions', db)
    
@admin_admissions_router.patch("/instructions/")
@auth_required(['staff'])
def update_admission_instructions(request:dict, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    return views.update_admission_content('admission_instructions', request['html'], db)
    

@public_admissions_router.patch("/forms/{form_id}/submissions/{submission_id}/")
def update_submission_by_id(form_id:int,submission_id:int,request:dict,  db:Session = Depends(database.get_db),website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    check_if_admin_or_owner(website_user,form_id, submission_id, db)
    return views.update_submission_by_id(form_id, submission_id,request)

@admin_admissions_router.get("/forms/", response_model=admission_schemas.AdmissionFormList)
def get_all_forms(db:Session = Depends(database.get_db)):
    return views.get_all_forms(db)

@unauthorized_admissions_router.get("/forms/active/", response_model=admission_schemas.AdmissionFormList)
def get_all_active_forms(db:Session = Depends(database.get_db)):
    return views.get_all_active_forms(db)

@admin_admissions_router.get("/forms/{form_id}/pdf-format/",)
@auth_required(['staff'])
def get_admission_html(form_id:int, website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    file_name = "patrick/admission" + str(form_id) +".html"
    html =  views.read_s3_file("admission-forms", file_name)
    return {"html": html}
    
@unauthorized_admissions_router.post("/forms/{form_id}/pdf-generate/sample/",)
def generate_sample_pdf(form_id:int, request:dict):
    file_name = "patrick/admission" + str(form_id) +".html"
    html =  views.read_s3_file("admission-forms", file_name)
    return views.generate_sample_pdf(html,request)    

@admin_admissions_router.post("/forms/{form_id}/pdf-generate/",)
def generate_admission_pdf(form_id:int, request:admission_schemas.PdfIDs,db:Session = Depends(database.get_db)):
    file_name = "patrick/admission" + str(form_id) +".html"
    html =  views.read_s3_file("admission-forms", file_name)
    return views.generate_pdfs(form_id, html,request.ids,db)
    
@admin_admissions_router.patch("/forms/{form_id}/pdf-format/",)
@auth_required(['staff'])
def patch_admissin_html(form_id:int, request:admission_schemas.AdmissionHtml, website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    file_name = "patrick/admission" + str(form_id) +".html"
    resp = views.patch_s3_file("admission-forms", file_name, request.html)
    return {"success": resp}

@admin_admissions_router.patch("/forms/{form_id}/public-configuration/",)
@auth_required(['staff'])
def patch_admissin_public_configuration(form_id:int, request:dict, website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user), db:Session = Depends(database.get_db)):
   
    return views.patch_admissin_public_configuration(form_id,request, db)

@admin_admissions_router.get("/forms/{form_id}/", response_model=admission_schemas.AdmissionFormSchema)
def get_admission_form(form_id:int, db:Session = Depends(database.get_db)):
    return views.get_admission_form(form_id,db)
    
@unauthorized_admissions_router.get("/forms/{form_id}/overview/", response_model = admission_schemas.AdmissionFormLimitedData)
def get_admission_form_limited_data(form_id:int, db:Session = Depends(database.get_db)):
    return views.get_admission_form(form_id,db)

@admin_admissions_router.post("/forms/", response_model = admission_schemas.AdmissionFormSchema)
@auth_required(['staff'])
def create_admission_form_template(request:admission_schemas.AdmissionFormRequestSchema, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    return views.create_admission_form_template(request,db)

@admin_admissions_router.patch("/forms/{id}/", response_model = admission_schemas.AdmissionFormSchema)
@auth_required(['staff'])
def update_admission_form(id:int, request:admission_schemas.AdmissionFormPatchRequestSchema, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    return views.update_admission_form_overview(id, request,db)

@admin_admissions_router.post("/submission/{id}/{status}/")
@auth_required(['staff'])
def update_application_detail_status(id:int, status, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    return updates.update_application_detail_status(id, status, db)

@unauthorized_admissions_router.get("/{form_name}/fields/",response_model=schemas.DataField)
def get_admission_fields(form_name:str, db:Session = Depends(database.get_db)):
    """
    Get admission fields of the school
    """
    fields = views.get_admission_fields(form_name, db)
    if not fields:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND)
    return fields

@admin_admissions_router.post("/{form_name}/sections/update/",response_model=schemas.DataField)
@auth_required(['staff'])
def update_admission_section(form_name:str, request:schemas.SectionUpdate,db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    """
    Update admission section of the school
    """
    table_name = "admission" + str(form_name)
    return views.update_admission_section(table_name, request, db)

@admin_admissions_router.get("/forms/{form_name}/transactions/",response_model=admission_schemas.ApplicationPaymentsList)
@auth_required(['staff'])
def get_all_transactions_for_form(form_name:str, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    """
    Update admission section of the school
    """
    return views.get_all_transactions_for_form(form_name, db)


@public_admissions_router.post("/transactions/",response_model=admission_schemas.ApplicationPaymentsList)
def get_all_transactions_for_user(request:admission_schemas.ApplicationInitiation, db:Session = Depends(database.get_db)):
    """
    Update admission section of the school
    """
    return views.get_all_transactions_for_user(request,'admissions', db)


@admin_admissions_router.patch("/forms/{form_name}/submissions/move/")
@auth_required(['staff'])
def update_stages_in_bulk(form_name:str, request:admission_schemas.StageMoveBulkRequest, db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    """
    Update admission stages for applicants
    """
    return views.update_stages_in_bulk(form_name, request, db)


@admin_admissions_router.post("/{form_name}/sections/save/",response_model=schemas.DataField)
@auth_required(['staff'])
def save_admission_section(form_name:str, request:schemas.SectionSave,db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    """
    Save admission section of the school
    """
    table_name = "admission" + str(form_name)
    return views.save_admission_section(table_name, request, db)

@admin_admissions_router.post("/{form_name}/fields/update/",response_model=schemas.DataField)
@auth_required(['staff'])
def update_admission_fields(form_name:str, request:schemas.DataFieldUpdate,db:Session = Depends(database.get_db), website_user: firebase.CustomClaims = Depends(firebase.get_current_firebase_user)):
    """
    Update admission fields of the school
    """
    table_name = "admission" + str(form_name)
    return views.update_admission_fields(table_name, request, db)
