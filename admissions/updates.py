from datetime import date, datetime, timezone
import uuid
from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy import func, select, column, text
from admissions import schemas
from admissions.models import AdmissionForms, AdmissionApplications
from admissions.schemas import AdmissionFormRequestSchema, AdmissionFormPatchRequestSchema, StageMoveBulkRequest
from admissions.constants import DEFAULT_SECTION
from payments.models import UserTransactions
from utils import data_fields, database, models, schemas


def get_application_overview(form_id:int,submission_id:int, db:Session):
    matched_forms = db.query(AdmissionApplications).filter(form_id == AdmissionApplications.form_id, submission_id == AdmissionApplications.application_id).all()
    if len(matched_forms) == 0:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return matched_forms[0]


def update_application_overview_status(application_id:int, status:str,db: Session):
    result_copy = db.query(AdmissionApplications).filter(application_id == AdmissionApplications.id)
    result = result_copy.first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Application Not Found")
    result_copy.update({"stage": status, "last_modified": datetime.now(tz=timezone.utc)})
    db.commit()
    db.refresh(result)
    return result

def update_application_detail_status(application_id:int, status:str, db: Session):
    print("Updating Detail")
    matched_forms = db.query(AdmissionApplications).filter(application_id == AdmissionApplications.id).all()
    if len(matched_forms) == 0:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    matched_form = matched_forms[0]
    table_name = "admission" + str(matched_form.form_id)
    result = database.update_record(table_name,matched_form.application_id, {"Stage":status})
    return result
