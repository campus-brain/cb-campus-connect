from datetime import date, datetime, timezone
import math
import re
from typing import List
import requests
import uuid
import boto3
# import pdfkit
from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from fastapi.param_functions import Body
import jinja2
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from sqlalchemy import func, select, column, text, Date, cast, DateTime

from admissions import schemas
from payments.schemas import RazorPayUserDetail
from payments.repo import update_records
from admissions.models import AdmissionForms, AdmissionApplications
from admissions.schemas import AdmissionFormRequestSchema, AdmissionFormPatchRequestSchema, StageMoveBulkRequest, ApplicationInitiation, CustomLinks
from admissions.constants import DEFAULT_SECTION
from payments.models import UserTransactions
from utils import data_fields, database, models, schemas
from admissions.updates import get_application_overview, update_application_overview_status, update_application_detail_status


def get_all_forms(db:Session):
    forms = db.query(AdmissionForms).all()
    if not forms:
        return {"form_list":[]}
    return {"form_list": forms}
def get_all_active_forms(db:Session):
    today = datetime.now(tz=timezone.utc)
    forms = db.query(AdmissionForms).filter(cast(AdmissionForms.start_date, DateTime) <= today).filter(cast(AdmissionForms.end_date, DateTime)>=today ).all()
    if not forms:
        return {"form_list":[]}
    return {"form_list": forms}

def get_admission_form(form_id:int, db:Session):
    matched_forms = db.query(AdmissionForms).filter(AdmissionForms.id == form_id).all()
    if len(matched_forms) == 0:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return matched_forms[0]

def get_all_submissions(visitor_id:str,db:Session):
    applications = db.query(AdmissionApplications,AdmissionForms) \
        .filter(visitor_id == AdmissionApplications.visitor_id) \
        .outerjoin(AdmissionForms, AdmissionForms.id == AdmissionApplications.form_id) \
        .all()
    if applications:
        return { "applications" : applications}
    return { "applications" : []}

def create_public_application_form(form_id:int, public_data:dict, db:Session):
    table_name = "admission" + str(form_id)
    admission_form:schemas.AdmissionFormLimitedData = get_admission_form(form_id,db)
    visitor_id_key = admission_form.public_configuration['visitorId']

    data = {
        "LoginID": public_data[visitor_id_key],
        "VisitorID": public_data[visitor_id_key],
        "InitiatedAt": datetime.now(tz=timezone.utc),
        "SubmittedAt": datetime.now(tz=timezone.utc),
        "Stage": 'submitted'
    }
    for field in public_data:
        data[field] = public_data[field]

    inserted_record_id = database.insert_record(table_name,data, db)
    if inserted_record_id:
        new_application = AdmissionApplications(
            login_id = public_data[visitor_id_key],
            visitor_id = public_data[visitor_id_key],
            form_id = form_id, 
            application_id=inserted_record_id, 
            stage = 'submitted',
            last_modified = datetime.now(tz=timezone.utc))
        db.add(new_application)
        db.commit()
        db.refresh(new_application)
        file_name = "patrick/admission" + str(form_id) +".html"
        html =  read_s3_file("admission-forms", file_name)
        pdf = generate_pdfs(form_id, html,[inserted_record_id], db, [data])
        return { "pdf": pdf, "record": new_application }
    raise HTTPException(status_code = status.HTTP_404_NOT_FOUND, detail = "Unable to insert record")


def create_application_form(form_id:int, visitor_id:str, login_id:str, db:Session):
    table_name = "admission" + str(form_id)
    data = {
        "LoginID": login_id,
        "VisitorID": visitor_id,
        "InitiatedAt": datetime.now(tz=timezone.utc),
        "Stage": 'not submitted'
    }

    inserted_record_id = database.insert_record(table_name,data, db)
    if inserted_record_id:
        new_application = AdmissionApplications(
            login_id = login_id,
            visitor_id = visitor_id,
         form_id = form_id, application_id=inserted_record_id, 
         stage = 'not submitted', last_modified = datetime.now(tz=timezone.utc))
        db.add(new_application)
        db.commit()
        return { "success": True }
    raise HTTPException(status_code = status.HTTP_404_NOT_FOUND, detail = "Unable to insert record")

def update_submission_by_id(form_id:int,submission_id:int,values:dict):
    table_name = "admission" + str(form_id)
    result = database.update_record(table_name, submission_id, values)
    return result


def update_admission_content(_type, html, db:Session):
    result_copy = db.query(models.DataFields).filter(models.DataFields.role_name == _type)
    result = result_copy.first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Role Not Found")
    result_copy.update({"sections":{"html": html}})
    db.commit()
    db.refresh(result)
    return result


def update_admission_custom_links(request:CustomLinks,db:Session):
    result_copy = db.query(models.DataFields).filter(models.DataFields.role_name == 'admission_custom_links')
    result = result_copy.first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Role Not Found")
    result_copy.update({"sections":{"links": jsonable_encoder(request.links)}})
    db.commit()
    db.refresh(result)
    return result

def get_admission_custom_links(db:Session):
    matched_forms = db.query(models.DataFields).filter(models.DataFields.role_name == 'admission_custom_links').all()
    if len(matched_forms) == 0:
        try:
            new_item = models.DataFields(role_name= 'admission_custom_links', sections={"links":[]})
            db.add(new_item)
            db.commit()
            db.refresh(new_item)
            return {"links":[]}
        except IntegrityError:
            raise HTTPException(status_code=400,detail="Duplicate item")
        admission_about_us
    return matched_forms[0].sections

def get_admission_content(_type:str, db:Session):
    matched_forms = db.query(models.DataFields).filter(models.DataFields.role_name == _type).all()
    if len(matched_forms) == 0:
        try:
            new_role = models.DataFields(role_name= _type, sections={"html":''})
            db.add(new_role)
            db.commit()
            db.refresh(new_role)
            return {"html":''}
        except IntegrityError:
            raise HTTPException(status_code=400,detail="Duplicate Role")
        admission_about_us
    return matched_forms[0].sections


def get_submission_by_id(form_id:int,submission_id:int, db:Session):
    table_name = "admission" + str(form_id)
    rows = get_submission_record(table_name, db, submission_id)
    rows = [k for k in rows]
    if len(rows) != 1:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Form Not Found")
    admission_columns = database.get_columns(table_name)

    i = 0
    row = rows[0]
    values = {}
    for column in admission_columns:
        values[column] = row[i]
        i += 1
    application = get_application_overview(form_id, submission_id, db)
    
    payments = db.query(UserTransactions).filter(UserTransactions.category_id == application.id).order_by(UserTransactions.id.desc()).first()
        
    return {"values":values,"payments":payments}

def lock_submission_by_id(form_id:int,submission_id:int,db:Session):
    try:
        application = get_application_overview(form_id, submission_id, db)
        if not application:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail="Form not found")
        update_application_overview_status(application.id, 'submitted', db)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,detail="Something went wrong")
    values = {
        "SubmittedAt": datetime.now(tz=timezone.utc),
        "Stage": "submitted"
    }
    return update_submission_by_id(form_id,submission_id,values)

def record_cash_transaction_by_submission_id(form_id:int,submission_id:int,request:RazorPayUserDetail, db:Session):
    try:
        form = db.query(AdmissionForms).filter(AdmissionForms.id == form_id).first()
        payment_data = {
            "razorpay_order_id": "order_offline_" + str(uuid.uuid4()),
            "razorpay_payment_id": "pay_offline_" + str(uuid.uuid4()),
        }
        order_details={
            "amount": form.fees * 100,
            "status":"captured"
        }
        print(request,payment_data, order_details)
        return update_records(request, payment_data, order_details, db)
    except Exception as e:
       print(e)
       raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,detail="Failed to record cash transaction")
   

def get_admission_fields(form_name:str, db:Session):
    form_name = "admission" + form_name
    return data_fields.get_fields(form_name, db)

def update_admission_form_overview(id:int, request: AdmissionFormPatchRequestSchema,db: Session):
    result_copy = db.query(AdmissionForms).filter(AdmissionForms.id == id)
    result = result_copy.first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Form Not Found")
    result_copy.update({
        "name":request.name,
        "start_date": request.start_date,
        "end_date": request.end_date,
        "description": request.description,
        "max_applications": request.max_applications,
        "login_required": request.login_required,
        "fees":request.fees,
    })
    db.commit()
    db.refresh(result)
    return result
   
def patch_admissin_public_configuration(id:int, request: dict,db: Session):
    result_copy = db.query(AdmissionForms).filter(AdmissionForms.id == id)
    result = result_copy.first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Form Not Found")
    result_copy.update({
        "public_configuration":request
    })
    db.commit()
    db.refresh(result)
    return result
    

def get_submission_record(table_name_string:str, db: Session, submission_id:int = None):
    selected_table = database.get_table(table_name_string)
    if submission_id:
        query = selected_table.select(whereclause=text(f'Id = "{submission_id}"'))
    else:
        query = selected_table.select()
    rows = db.execute(query)
    return rows

def get_submission_record_with_where_clause(table_name_string:str, db: Session, where_clauses = None):
    selected_table = database.get_table(table_name_string)
    if where_clauses:
        _text = ''
        i = 0
        j = len(where_clauses)
        for clause in where_clauses:
            i += 1
            if i == j:
                _text += f'{clause} = "{where_clauses[clause]}"'
            else:
                _text += f'{clause} = "{where_clauses[clause]}" and '
            
        query = selected_table.select(whereclause=text(_text))
    else:
        query = selected_table.select()
    rows = db.execute(query)
    return rows
    
def get_submission_records_in_set(table_name_string:str, db: Session, ranges):
    selected_table = database.get_table(table_name_string)
    records = [f'\"{record}\"' for record in ranges]
    record_text = ",".join(records)
    query = selected_table.select(whereclause=text(f'Id in ({record_text})'))
    rows = db.execute(query)
    return rows


def get_public_submissions(form_id:int, request:dict, db:Session):
    admission_form:schemas.AdmissionFormLimitedData = get_admission_form(form_id,db)
    public_configuration = admission_form.public_configuration['keys']
    keys_match = True
    for config in public_configuration:
        if config['field']['id'] not in request:
            keys_match = False
            break
    if not keys_match:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail='Wrong/Missing key')
    table_name_string = "admission"+str(form_id)
    rows = get_submission_record_with_where_clause(table_name_string, db, request)
    result = []

    admission_columns = database.get_columns(table_name_string)
    for row in rows:
        # result = [k for k in row]
        i = 0
        values = {}
        for column in admission_columns:
            values[column] = row[i]
            i += 1
        result.append(values)
        
    if len(result) == 0:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail='No Matching detail')
    
    file_name = "patrick/admission" + str(form_id) +".html"
    html =  read_s3_file("admission-forms", file_name)
    pdf = generate_pdfs(form_id, html,[result[0]['Id']],db, result)
    return {"pdf":pdf, "record":result[0]}

def get_submissions(form_id:int, db:Session):
    table_name_string = "admission"+str(form_id)
    rows = get_submission_record(table_name_string, db)
    result = []

    admission_columns = database.get_columns(table_name_string)
    for row in rows:
        # result = [k for k in row]
        i = 0
        values = {}
        for column in admission_columns:
            values[column] = row[i]
            i += 1
        result.append(values)
        
    return result

def get_submissions_in_range(form_id:int, db:Session, ranges):
    table_name_string = "admission"+str(form_id)
    rows = get_submission_records_in_set(table_name_string, db, ranges=ranges)
    result = []

    admission_columns = database.get_columns(table_name_string)
    for row in rows:
        # result = [k for k in row]
        i = 0
        values = {}
        for column in admission_columns:
            values[column] = row[i]
            i += 1
        result.append(values)
        
    return result

def print_checkbox(user_input, option):
    return f'''<span class="inline-block"><label class="control control--checkbox"><input type="checkbox" {'checked' if user_input == option else ''}/><div class="control__indicator"></div>{option}</label></span>'''

def print_others_checkbox(user_input, options):
    return f'''<span class="inline-block"><label class="control control--checkbox"><input type="checkbox" {'checked' if user_input not in options else ''}/><div class="control__indicator"></div>Others <u>  {user_input}  </u></label></span>'''

def get_age(birthday):
    if birthday:
        _today = date.today()
        _birth_day = datetime.strptime(birthday,"%Y-%m-%d").date()
        total_days = (_today - _birth_day).days
        years = total_days/365.25
        return f"{math.floor(years)} Years {math.floor((years - math.floor(years))*12)} Months"
    return ''


def checkNoneString(value):
    if value is None or value == 'None':
        return '         '
    return value

jinja2.filters.FILTERS['get_age'] = get_age
jinja2.Environment(finalize=checkNoneString)

custom_functions = {
    "print_checkbox":print_checkbox,
    "print_others_checkbox":print_others_checkbox
}

def generate_sample_pdf(html, data):
    try:
        f = open("utils/template.html", "r")
    except Exception as e:
        print("File failed")
        print(e)
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail="Failed to read file")
    individual_html = jinja2.Template(html).render(**data)
    individual_html = re.sub(r"width: [0-9]+px;", "width: 100%", individual_html)
    _html = jinja2.Template(f.read()).render(addCodeHere=individual_html)
    # _html = jinja2.Template(f.read()).render(**data)
    options = {
        'page-size': 'Legal',
        'margin-top': '0.1in',
        'margin-right': '0.15in',
        'margin-bottom': '0.1in',
        'margin-left': '0.15in',
        'encoding': "UTF-8",
        'no-outline': None
    }
    # pdfkit.from_string(_html, 'spacetest.pdf',options=options)
    try:
        pdf_url = "https://fwtbmhchqg.execute-api.ap-south-1.amazonaws.com/dev/api/htmltopdf"
        resp = requests.post(pdf_url, { "html" : _html, "file": str(uuid.uuid4()) })
        return resp.json()
    except Exception as e:
        print("Failed because of ")
        print(str(e))
        return False

def generate_pdfs(form_id, html:str,ids:List[int], db:Session, records = None):
    try:
        f = open("utils/template.html", "r")
    except Exception as e:
        print("File failed")
        print(e)
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,detail="Failed to read file")
    
    _html = ""
    if not records:
        records = get_submissions_in_range(form_id=form_id,db=db,ranges=ids)
    i = 0
    j = len(records)
    for record in records:
        i += 1
        
        env = jinja2.Environment(finalize=checkNoneString)
        individual_html = env.from_string(html, template_class=jinja2.Template).render(**record,**custom_functions)
        # _html = re.sub('<table class=\\"quill-better-table\\" style=\\"width: [0-9]+px;\\"><colspan>','<table class=\"quill-better-table\" style=\"100%;\"><colspan>', _html)

        individual_html = re.sub(r"width: [0-9]+px;", "width: 100%", individual_html)
        individual_html = re.sub(r"\t", "&nbsp;&nbsp;", individual_html)
        if i != j:
            individual_html = individual_html + "<div class='pb_after'></div>"
        _html += individual_html
   
    _html = jinja2.Template(f.read()).render(addCodeHere=_html)
   
    try:
        pdf_url = "https://fwtbmhchqg.execute-api.ap-south-1.amazonaws.com/dev/api/htmltopdf"
        resp = requests.post(pdf_url, { "html" : _html, "file":str(uuid.uuid4()) })
        return resp.json()
    except Exception as e:
        print("Failed because of ")
        print(str(e))
        return False

def read_s3_file(bucket_name, file_name):
    try:
        s3 = boto3.client('s3')
        obj = s3.get_object(Bucket=bucket_name, Key=file_name)
        return obj['Body'].read().decode('utf-8')
    except Exception as e:
        print("Failed to read s3 file")
        print(e)
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Failed to read s3 file")
def patch_s3_file(bucket_name, file_name, body):
    try:
        s3 = boto3.client('s3')
        s3.put_object(
            Bucket=bucket_name,
            Key = file_name,
            Body = body
        )
        return True
    except Exception as e:
        print("Failed to read s3 file")
        print(e)
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Failed to read s3 file")

def create_s3_file(bucket_name, file_name):
    try:
        s3 = boto3.client('s3')
        s3.put_object(
            Bucket=bucket_name,
            Key=file_name
        )
        return True
    except Exception as e:
        print("Failed to create s3 file")
        print(e)
        return False


def create_admission_form_template(request: AdmissionFormRequestSchema , db:Session):
    new_admission_form = AdmissionForms(
        name = request.name,
        start_date = request.start_date,
        end_date =request.end_date,
        max_applications = request.max_applications,
        login_required = request.login_required,
        fees = request.fees,
        description = request.description,
        created_by = request.created_by,
        created_at = datetime.now(tz=timezone.utc),
        configuration = {},
        public_configuration = {
            'visitorId': 'username' if request.login_required else '',
            'keys': []
        },
    )
    db.add(new_admission_form)
    db.commit()
    db.refresh(new_admission_form)

    table_name = "admission" + str(new_admission_form.id)
    asked_template = request.template
    template = {"sections":[]}
    if asked_template == 'default':
        template = DEFAULT_SECTION
    elif asked_template == 'empty':
        template = {"sections":[]}
    else:
        template_name = "admission" + asked_template
        matched_template = db.query(models.DataFields).filter(models.DataFields.role_name == template_name).first()
        template = matched_template.sections
    # create sections
    sections_record = models.DataFields(role_name = table_name, sections = template)
    db.add(sections_record)
    db.commit()
    db.refresh(sections_record)

    # create table
    is_created = database.create_table_for_admissions(table_name, template)
    if not is_created:
        db.query(AdmissionForms).filter(AdmissionForms.id==new_admission_form.id).delete()
        db.commit()
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)
    resp = create_s3_file('admission-forms', f"patrick/{table_name}.html")
    if not resp:
        print("Failed to create admission html")
    return new_admission_form

def update_admission_fields(table_name:str, request:schemas.DataFieldUpdate,db:Session):
    return data_fields.update_fields(table_name, table_name, request,db)



def save_admission_section(table_name:str, request:schemas.SectionSave, db:Session):
    result_copy = db.query(models.DataFields).filter(models.DataFields.role_name == table_name)
    result = result_copy.first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Form Not Found")
    result_copy.update({"sections": {"sections":request.sections}})
    db.commit()
    db.refresh(result)
    return result
   
def update_stages_in_bulk(form_name:str, request:StageMoveBulkRequest, db:Session):
    table_name = "admission" + str(form_name)
    resp = database.update_record_in_bulk(table_name, request.ids, {"Stage":request.stage})
    if not resp:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Failed to update records")
    try:
        result_copy = db.query(AdmissionApplications).filter(AdmissionApplications.form_id == form_name, AdmissionApplications.application_id.in_(request.ids))
        result_copy.update({"stage": request.stage})
        db.commit()
        return True
    except Exception as e:
        print(e)
        return False
   
    
    

def update_admission_section(table_name:str, request:schemas.SectionUpdate,db:Session):
    is_additional_table = True
    return data_fields.update_sections(table_name, table_name, request, is_additional_table ,db)
   
def get_all_transactions_for_form(form_id,db:Session):
    
    forms = db.query(UserTransactions,AdmissionApplications).outerjoin(AdmissionApplications, AdmissionApplications.id == UserTransactions.category_id).filter(AdmissionApplications.form_id==form_id).all()
    if not forms:
        return {"payments":[]}
    return {"payments": forms}

def get_all_transactions_for_user(request:ApplicationInitiation, category,db:Session):
    
    forms = db.query(UserTransactions, AdmissionApplications).filter(UserTransactions.user_id==request.visitor_id,UserTransactions.category == category).outerjoin(AdmissionApplications, AdmissionApplications.visitor_id==UserTransactions.user_id).all()
    if not forms:
        return {"payments":[]}
    return {"payments": forms}