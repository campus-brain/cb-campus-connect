from datetime import date
from typing import List
from fastapi import APIRouter, Depends, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from academic.attendance import models, schemas, attendance
from utils import database
from utils.database import get_db

attendance_router = APIRouter(tags=['Academic - Attendance'], prefix='/attendance')



@attendance_router.get('/student/{student_id}/{course_id}/{batch_id}/{academic_year}', status_code=status.HTTP_200_OK)
def get_student_attendance_history_entity_level(student_id:str, course_id:str, batch_id:str, academic_year:str, db: Session = Depends(get_db)):
    """
    Get Attendance history for a student on an Academic Year - Entity level
    """
    return attendance.get_student_attendance_history_entity_level(student_id, course_id, batch_id, academic_year, db)

@attendance_router.get('/student/{student_id}/{course_id}/{batch_id}/{academic_year}/{date_indicator}', status_code=status.HTTP_200_OK)
def get_student_attendance_history_entity_level_by_day(student_id:str, course_id:str, batch_id:str, academic_year:str, date_indicator:date, db: Session = Depends(get_db)):
    """
    Get Attendance history for a student on an Academic Year - Entity level on a given day
    """
    return attendance.get_student_attendance_history_entity_level_by_day(student_id, course_id, batch_id, academic_year, date_indicator, db)

@attendance_router.get('/batch/{course_id}/{batch_id}/{academic_year}', status_code=status.HTTP_200_OK)
def get_batch_attendance_history(course_id:str, batch_id:str, academic_year:str, db: Session = Depends(get_db)):
    """
    Get Attendance history for a batch on an Academic Year
    """
    return attendance.get_batch_attendance_history(course_id, batch_id, academic_year, db)

@attendance_router.get('/batch/{course_id}/{batch_id}/{academic_year}/{date_indicator}', status_code=status.HTTP_200_OK)
def get_batch_attendance_history_by_date(course_id:str, batch_id:str, academic_year:str, date_indicator: date, db: Session = Depends(get_db)):
    """
    Batch attendance on a given day on an Academic Year
    """
    return attendance.get_batch_attendance_history_by_date(course_id, batch_id, academic_year, date_indicator, db)

@attendance_router.get('/batch-aggregate/{course_id}/{batch_id}/{academic_year}', status_code=status.HTTP_200_OK)
def get_batch_aggregate_attendance_history(course_id:str, batch_id:str, academic_year:str, db: Session = Depends(get_db)):
    """
    Batch Cumulative attendance
    """
    return attendance.get_batch_aggregate_attendance_history(course_id, batch_id, academic_year, db)

@attendance_router.get('/batch-aggregate/{course_id}/{batch_id}/{academic_year}/{date_indicator}', status_code=status.HTTP_200_OK)
def get_batch_aggregate_attendance_history_by_date(course_id:str, batch_id:str, academic_year:str, date_indicator: date, db: Session = Depends(get_db)):
    """
    Batch Cumulative attendance on a given day
    """
    return attendance.get_batch_aggregate_attendance_history_by_date(course_id, batch_id, academic_year, date_indicator, db)


@attendance_router.post('/mark-attendance', status_code=status.HTTP_201_CREATED)
def mark_attendance(request: List[schemas.BatchwiseAttendance], db: Session = Depends(get_db)):
    """
    Mark attendance for the given session(FN/AN) or session
    """
    return attendance.mark_attendance(request, db)

@attendance_router.patch('/mark-attendance', status_code=status.HTTP_201_CREATED)
def update_attendance(request: List[schemas.BatchwiseAttendance], db: Session = Depends(get_db)):
    """
    Mark attendance for the given session(FN/AN) or session
    """
    return attendance.update_attendance(request, db)

@attendance_router.post('/aggregate-student-attendance-by-day', status_code=status.HTTP_201_CREATED)
def aggregate_student_attendance_by_day(request: schemas.AggregateStudentAttendanceByDay, db: Session = Depends(get_db)):
    """
    Async function, invoked by cron or trigger - insert aggregates to DB
    """
    return attendance.aggregate_student_attendance_by_day(request, db)


"""
@attendance_router.post('/daily-unit', status_code=status.HTTP_201_CREATED)
def attendance_unit(request: schemas.BatchAttendanceDailyUnit, db: Session = Depends(get_db)):

    return attendance.attendance_unit(request, db)

@attendance_router.post('/monthly-unit', status_code=status.HTTP_201_CREATED)
def attendance_monthly_unit(request: schemas.BatchAttendanceMonthlyUnit, db: Session = Depends(get_db)):

    return attendance.attendance_monthly_unit(request, db)
"""