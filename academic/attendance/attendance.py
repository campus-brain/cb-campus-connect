from typing import List
from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from sqlalchemy import Table, MetaData

from academic.attendance import schemas, models
from utils.hash import Hash
from utils.database import add_column, has_table, get_operations
from datetime import date, datetime, timedelta
from alembic import op as operation
from sqlalchemy import extract
from sqlalchemy.sql import functions


def is_date_valid(date_str):
    try:
        datetime.strptime(date_str, '%d-%m-%Y')
    except ValueError:
        return False
    return True


def str_to_date(date_str):
    return datetime.strptime(date_str, '%d-%m-%Y')


def get_dates_between(start_date, end_date):
    start_date = str_to_date(start_date)
    end_date = str_to_date(end_date)
    if start_date > end_date:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid arguments")
    return [start_date + timedelta(days=x) for x in range((end_date-start_date).days)]


def get_student_attendance_history_entity_level(student_id: str, course_id: str, batch_id: str, academic_year: str, db: Session):
    result = db.query(models.StudentAttendanceDailyTracker).filter(
        models.StudentAttendanceDailyTracker.student_id == student_id
    ).filter(
        models.StudentAttendanceDailyTracker.academic_year == academic_year
    ).filter(
        models.StudentAttendanceDailyTracker.course_id == course_id
    ).filter(
        models.StudentAttendanceDailyTracker.batch_id == batch_id
    ).all()
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="No record found")

    return result


def get_student_attendance_history_entity_level_by_day(student_id: str, course_id: str, batch_id: str, academic_year: str, date_indicator: date, db: Session):
    result = db.query(models.StudentAttendanceDailyTracker).filter(
        models.StudentAttendanceDailyTracker.student_id == student_id
    ).filter(
        models.StudentAttendanceDailyTracker.academic_year == academic_year
    ).filter(
        models.StudentAttendanceDailyTracker.course_id == course_id
    ).filter(
        models.StudentAttendanceDailyTracker.batch_id == batch_id
    ).filter(
        models.StudentAttendanceDailyTracker.date_indicator == date_indicator
    ).all()
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="No record found")

    return result


def get_batch_attendance_history(course_id: str, batch_id: str, academic_year: str, db: Session):
    result = db.query(models.StudentAttendanceDailyTracker).filter(
        models.StudentAttendanceDailyTracker.academic_year == academic_year
    ).filter(
        models.StudentAttendanceDailyTracker.course_id == course_id
    ).filter(
        models.StudentAttendanceDailyTracker.batch_id == batch_id
    ).all()
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="No record found")

    return result


def get_batch_attendance_history_by_date(course_id: str, batch_id: str, academic_year: str, date_indicator: date, db: Session):

    result = db.query(models.StudentAttendanceDailyTracker).filter(
        models.StudentAttendanceDailyTracker.academic_year == academic_year
    ).filter(
        models.StudentAttendanceDailyTracker.course_id == course_id
    ).filter(
        models.StudentAttendanceDailyTracker.batch_id == batch_id
    ).filter(
        models.StudentAttendanceDailyTracker.date_indicator == date_indicator
    ).all()
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="No record found")

    return result


def mark_attendance(attendance_records: List[schemas.BatchwiseAttendance], db: Session):

    records = list()
    for record in attendance_records:
        attendance_record = models.StudentAttendanceDailyTracker(
            course_id=record.course_id, batch_id=record.batch_id,
            academic_year=record.academic_year,  student_id=record.student_id,
            date_indicator=record.date_indicator, entity_id=record.entity_id,
            entity_status=record.entity_status)
        records.append(attendance_record)

    db.add_all(records)
    db.commit()

    return "Updated successfully"


def update_attendance(attendance_records: List[schemas.BatchwiseAttendance], db: Session):
    records = list()

    for record in attendance_records:
        records.append(record.dict())

    db.bulk_update_mappings(models.StudentAttendanceDailyTracker, records)
    db.commit()

    return "Updated successfully"


def aggregate_student_attendance_by_day(request: schemas.AggregateStudentAttendanceByDay, db: Session):
    student_day_aggregate = db.query(
        models.StudentAttendanceDailyTracker.course_id.label("course_id"),
        models.StudentAttendanceDailyTracker.batch_id.label("batch_id"),
        models.StudentAttendanceDailyTracker.academic_year.label(
            "academic_year"),
        models.StudentAttendanceDailyTracker.student_id.label("student_id"),
        functions.sum(models.StudentAttendanceDailyTracker.entity_status)/functions.count(models.StudentAttendanceDailyTracker.entity_status)).filter(
        models.StudentAttendanceDailyTracker.date_indicator == request.date_indicator
    ).group_by("course_id", "batch_id", "academic_year", "student_id").all()
    keys = ["course_id", "batch_id",
            "academic_year", "student_id", "day_status"]
    student_day_aggregate_list = list()
    for row in student_day_aggregate:
        item_dict = {}
        for item in range(0, len(row)):
            item_dict[keys[item]] = row[item]
        item_dict["date_indicator"] = request.date_indicator

        student_day_aggregate_list.append(item_dict)
    db.bulk_insert_mappings(
        models.StudentAttendanceDailyAggregateTracker, student_day_aggregate_list)

    entity_day_aggregate = db.query(
        models.StudentAttendanceDailyTracker.course_id.label("course_id"),
        models.StudentAttendanceDailyTracker.batch_id.label("batch_id"),
        models.StudentAttendanceDailyTracker.academic_year.label(
            "academic_year"),
        models.StudentAttendanceDailyTracker.entity_id.label("entity_id"),
        functions.sum(models.StudentAttendanceDailyTracker.entity_status)/functions.count(models.StudentAttendanceDailyTracker.entity_status)).filter(
        models.StudentAttendanceDailyTracker.date_indicator == request.date_indicator
    ).group_by("course_id", "batch_id", "academic_year", "entity_id").all()
    keys = ["course_id", "batch_id",
            "academic_year", "entity_id", "day_status"]
    entity_day_aggregate_list = list()
    for row in entity_day_aggregate:
        item_dict = {}
        for item in range(0, len(row)):
            item_dict[keys[item]] = row[item]
        item_dict["date_indicator"] = request.date_indicator

        entity_day_aggregate_list.append(item_dict)
    db.bulk_insert_mappings(
        models.EntityAttendanceDailyAggregateTracker, entity_day_aggregate_list)
    db.commit()

    batch_day_aggregate = db.query(
        models.StudentAttendanceDailyAggregateTracker.course_id.label(
            "course_id"),
        models.StudentAttendanceDailyAggregateTracker.batch_id.label(
            "batch_id"),
        models.StudentAttendanceDailyAggregateTracker.academic_year.label(
            "academic_year"),
        functions.sum(models.StudentAttendanceDailyAggregateTracker.day_status)/functions.count(models.StudentAttendanceDailyAggregateTracker.day_status)).filter(
        models.StudentAttendanceDailyAggregateTracker.date_indicator == request.date_indicator
    ).group_by("course_id", "batch_id", "academic_year").all()
    keys = ["course_id", "batch_id", "academic_year", "day_status"]
    batch_day_aggregate_list = list()
    for row in batch_day_aggregate:
        item_dict = {}
        for item in range(0, len(row)):
            item_dict[keys[item]] = row[item]
        item_dict["date_indicator"] = request.date_indicator

        batch_day_aggregate_list.append(item_dict)
    db.bulk_insert_mappings(
        models.BatchAttendanceDailyAggregateTracker, batch_day_aggregate_list)
    db.commit()

    return "Updated successfully"


def attendance_unit(request: schemas.BatchAttendanceDailyUnit, db: Session):
    unit = models.BatchAttendanceDailyUnit(
        course_id=request.course_id, batch_id=request.batch_id,
        academic_year=request.academic_year,
        date_indicator=request.date_indicator, unit=request.unit,
        reason=request.reason)
    db.add(unit)
    db.commit()
    db.refresh(unit)
    return "Updated successfully"


def attendance_monthly_unit(request: schemas.BatchAttendanceMonthlyUnit, db: Session):

    monthly_unit = db.query(functions.sum(models.BatchAttendanceDailyUnit.unit)).filter(
        models.BatchAttendanceDailyUnit.academic_year == request.academic_year
    ).filter(
        models.BatchAttendanceDailyUnit.course_id == request.course_id
    ).filter(
        models.BatchAttendanceDailyUnit.batch_id == request.batch_id
    ).filter(
        extract(
            'month', models.BatchAttendanceDailyUnit.date_indicator) == request.month_indicator
    ).filter(
        extract(
            'year', models.BatchAttendanceDailyUnit.date_indicator) == request.year_indicator
    ).first()

    result = db.query(models.BatchAttendanceMonthlyUnit).filter(
        models.BatchAttendanceMonthlyUnit.academic_year == request.academic_year
    ).filter(
        models.BatchAttendanceMonthlyUnit.course_id == request.course_id
    ).filter(
        models.BatchAttendanceMonthlyUnit.batch_id == request.batch_id
    ).filter(
        models.BatchAttendanceMonthlyUnit.month_indicator == request.month_indicator
    ).filter(
        models.BatchAttendanceMonthlyUnit.year_indicator == request.year_indicator
    ).first()

    if not result:

        unit = models.BatchAttendanceMonthlyUnit(
            course_id=request.course_id, batch_id=request.batch_id,
            academic_year=request.academic_year,
            month_indicator=request.month_indicator, year_indicator=request.year_indicator,
            units=monthly_unit[0]
        )
        db.add(unit)
        db.commit()
        db.refresh(unit)
        return "Updated successfully"

    print("Record exists")
    db.query(models.BatchAttendanceMonthlyUnit).filter(
        models.BatchAttendanceMonthlyUnit.academic_year == request.academic_year
    ).filter(
        models.BatchAttendanceMonthlyUnit.course_id == request.course_id
    ).filter(
        models.BatchAttendanceMonthlyUnit.batch_id == request.batch_id
    ).filter(
        models.BatchAttendanceMonthlyUnit.month_indicator == request.month_indicator
    ).filter(
        models.BatchAttendanceMonthlyUnit.year_indicator == request.year_indicator
    ).update({models.BatchAttendanceMonthlyUnit.units: monthly_unit[0]})

    return "Updated successfully"


def get_batch_aggregate_attendance_history(course_id: str, batch_id: str, academic_year: str, db: Session):
    result = db.query(models.BatchAttendanceDailyAggregateTracker).filter(
        models.BatchAttendanceDailyAggregateTracker.academic_year == academic_year
    ).filter(
        models.BatchAttendanceDailyAggregateTracker.course_id == course_id
    ).filter(
        models.BatchAttendanceDailyAggregateTracker.batch_id == batch_id
    ).all()
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="No record found")

    return result


def get_batch_aggregate_attendance_history_by_date(course_id: str, batch_id: str, academic_year: str, date_indicator: date, db: Session):

    result = db.query(models.BatchAttendanceDailyAggregateTracker).filter(
        models.BatchAttendanceDailyAggregateTracker.academic_year == academic_year
    ).filter(
        models.BatchAttendanceDailyAggregateTracker.course_id == course_id
    ).filter(
        models.BatchAttendanceDailyAggregateTracker.batch_id == batch_id
    ).filter(
        models.BatchAttendanceDailyAggregateTracker.date_indicator == date_indicator
    ).all()
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="No record found")

    return result
