from sqlalchemy import Boolean, Column, Integer, String, true
from sqlalchemy.sql.schema import ForeignKeyConstraint, PrimaryKeyConstraint
from sqlalchemy.sql.sqltypes import DATE, Date, Float

from utils.database import Base


class StudentAttendanceDailyTracker(Base):
    __tablename__ = 'StudentAttendanceDailyTracker20212022'

    course_id = Column('CourseID', String(5))
    batch_id = Column('BatchID', String(20))
    academic_year = Column('AcademicYear', String(10))
    student_id = Column('StudentID', String(20))
    date_indicator = Column('DateIndicator', Date)
    entity_id = Column('EntityID', String(20))
    entity_status = Column('EntityStatus', Float)
    marked_by = Column('MarkedBy')

    PrimaryKeyConstraint(student_id, course_id,batch_id, academic_year, date_indicator, entity_id)

class StudentAttendanceDailyAggregateTracker(Base):
    __tablename__ = 'StudentAttendanceDailyAggregateTracker20212022'

    course_id = Column('CourseID', String(5))
    batch_id = Column('BatchID', String(20))
    academic_year = Column('AcademicYear', String(10))
    student_id = Column('StudentID', String(20))
    date_indicator = Column('DateIndicator', Date)
    day_status = Column("DayStatus", Float)

    PrimaryKeyConstraint(student_id, course_id,batch_id, academic_year, date_indicator)

class EntityAttendanceDailyAggregateTracker(Base):
    __tablename__ = 'EntityAttendanceDailyAggregateTracker'

    course_id = Column('CourseID', String(5))
    batch_id = Column('BatchID', String(20))
    academic_year = Column('AcademicYear', String(10))
    entity_id = Column('EntityID', String(20))
    date_indicator = Column('DateIndicator', Date)
    day_status = Column("DayStatus", Float)

    PrimaryKeyConstraint(course_id, batch_id, academic_year, date_indicator, entity_id)


class BatchAttendanceDailyAggregateTracker(Base):
    __tablename__ = 'BatchAttendanceDailyAggregateTracker'

    course_id = Column('CourseID', String(5))
    batch_id = Column('BatchID', String(20))
    academic_year = Column('AcademicYear', String(10))
    date_indicator = Column('DateIndicator', Date)
    day_status = Column('DayStatus', Float)

    PrimaryKeyConstraint( course_id,batch_id, academic_year, date_indicator)


class BatchAttendanceMonthlyUnit(Base):
    __tablename__ = 'BatchAttendanceMonthlyUnit'

    course_id = Column('CourseID', String(5))
    batch_id = Column('BatchID', String(20))
    academic_year = Column('AcademicYear', String(10))
    month_indicator = Column('MonthIndicator', Integer)
    year_indicator = Column('YearIndicator', Integer)
    units = Column('Units', Float)

    PrimaryKeyConstraint( course_id,batch_id, academic_year, month_indicator, year_indicator)

