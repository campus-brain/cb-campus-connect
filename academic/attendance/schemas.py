from datetime import date
from typing import List, Optional

from humps import camelize
from pydantic import BaseModel

class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class AttendanceConfig(CamelModel):
    start_date: str
    end_date: str
    class Config:
        orm_mode=True

class AttendanceByStudent(CamelModel):
    course_id : str
    batch_id : str
    academic_year : str
    student_id : str
    class Config:
        orm_mode=True

class BatchwiseAttendance(CamelModel):
    course_id : str
    batch_id : str
    academic_year : str
    student_id : str
    date_indicator : date
    entity_id: str
    entity_status: float
    class Config:
        orm_mode=True

class AggregateStudentAttendanceByDay(CamelModel):
    date_indicator: date
    class Config:
        orm_mode=True


class BatchAttendanceDailyUnit(CamelModel):
    course_id : str
    batch_id : str
    academic_year : str
    date_indicator : date
    unit: float
    reason: str
    class Config:
        orm_mode=True

class BatchAttendanceMonthlyUnit(CamelModel):
    course_id : str
    batch_id : str
    academic_year : str
    month_indicator : int
    year_indicator: int
    units: float
    class Config:
        orm_mode=True