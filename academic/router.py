from typing import Optional

from fastapi import APIRouter, Depends, Header, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from academic.batch.router import batch_router
from academic.course.router import course_router
from academic.subject.router import subject_router
from academic.attendance.router import attendance_router
from academic.academic_year.router import academic_year_router
from academic.staff_subject.router import staff_subject_router
from academic.schedule_info.router import schedule_info_router
from academic.test_module.router import test_module_router


academic_router = APIRouter(prefix='/academic')
academic_router.include_router(batch_router)
academic_router.include_router(course_router)
academic_router.include_router(subject_router)
academic_router.include_router(attendance_router)
academic_router.include_router(academic_year_router)
academic_router.include_router(staff_subject_router)
academic_router.include_router(schedule_info_router)
academic_router.include_router(test_module_router)