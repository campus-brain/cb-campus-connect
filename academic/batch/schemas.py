from typing import List, Optional

from humps import camelize
from pydantic import BaseModel
from admin.schemas import Student

class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class Batch(CamelModel):
    course_id: str
    batch_id: str
    academic_year: str

    class Config:
        orm_mode = True


class StudentBatchTracker(CamelModel):
    course_id: str
    batch_id: str
    academic_year: str
    roll_number: str
    student_id: str

    class Config:
        orm_mode = True

class StudentBatchTrackerWithIntro(CamelModel):
    StudentBatchTracker: StudentBatchTracker
    Student: Student

class StudentsList(CamelModel):
    students: List[StudentBatchTrackerWithIntro]

class BatchList(CamelModel):
    batches: List[Batch]