from fastapi import APIRouter, Depends, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from academic.batch import models, schemas, batch
from utils import database
from utils.database import get_db
from typing import Optional

batch_router = APIRouter(tags=['Academic - Batch'], prefix='/batch')


@batch_router.post('/create/', status_code=status.HTTP_201_CREATED)
def create_batch(request: schemas.Batch, db: Session = Depends(get_db)):
    """
    Create Batch
    """
    return batch.create(request, db)

@batch_router.get('/', status_code=status.HTTP_200_OK, response_model=schemas.BatchList)
def get_all_batch(db: Session = Depends(get_db)):
    """
    Get batches
    """
    return batch.get_all_batches(db)

@batch_router.get('/tracker/{student_id}/', status_code=status.HTTP_200_OK, response_model=schemas.StudentBatchTracker)
def get_student_batch_history(student_id: str, db: Session = Depends(get_db)):
    """
    Get Student batch
    """
    return batch.get_student_batch_history(student_id, db)

# , response_model=schemas.StudentsList
@batch_router.get('/list/', status_code=status.HTTP_200_OK, response_model=schemas.StudentsList)
def get_students_list(academic_year:Optional[str]=None, course_id:Optional[str]=None, batch_id:Optional[str]=None,page:int=1, db: Session = Depends(get_db)):
    return batch.get_students_list(academic_year, course_id, batch_id,page, db)