from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy import select
from sqlalchemy.sql import text
from academic.batch import schemas, models
from admin.models import Student
from utils.hash import Hash
from utils.database import get_columns

def create(request: schemas.Batch, db: Session):
    new_batch = models.BatchData(
        course_id = request.course_id, batch_id = request.batch_id, academic_year = request.academic_year,
        active=True)
    db.add(new_batch)
    db.commit()
    db.refresh(new_batch)
    return new_batch

def get_all_batches(db: Session):
    return {"batches": db.query(models.BatchData).all()} 

def get_student_batch_history(student_id:str, db:Session):
    result = db.query(models.StudentBatchTracker).filter(models.StudentBatchTracker.student_id == student_id).first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    return result    

def get_students_list(academic_year:str, course_id:str, batch_id:str,page:int, db:Session):
    page_size = 25
    res = db.query(models.StudentBatchTracker, Student) \
        .filter(not academic_year or (academic_year and models.StudentBatchTracker.academic_year == academic_year)) \
        .filter(not course_id or (course_id and course_id!="" and models.StudentBatchTracker.course_id == course_id)) \
        .filter(not batch_id or (batch_id and batch_id!="" and models.StudentBatchTracker.batch_id == batch_id)) \
        .outerjoin(Student,models.StudentBatchTracker.student_id == Student.student_id) \
        .offset((page-1)*page_size) \
        .limit(page_size).all()

    return {"students": jsonable_encoder(res)}