from sqlalchemy import Boolean, Column, Integer, String, true, ForeignKey
from sqlalchemy.sql.schema import ForeignKeyConstraint, PrimaryKeyConstraint

from utils.database import Base


class BatchData(Base):
    __tablename__ = 'BatchData'

    course_id = Column('CourseID', String(5))
    batch_id = Column('BatchID', String(20))
    academic_year = Column('AcademicYear', String(10))
    PrimaryKeyConstraint(course_id,batch_id, academic_year)

class StudentBatchTracker(Base):
    __tablename__ = 'StudentBatchTracker'

    course_id = Column('CourseID', String(5))
    batch_id = Column('BatchID', String(20))
    academic_year = Column('AcademicYear', String(10))
    roll_number = Column('RollNo', String(20))
    student_id = Column('StudentID', String(25), ForeignKey("Student.StudentID"))
    
    PrimaryKeyConstraint(course_id,batch_id, academic_year, student_id)