from typing import List
from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from sqlalchemy import Table, MetaData

from academic.test_module import schemas, models
from utils.hash import Hash
from utils.database import add_column, has_table, get_operations
from datetime import date, datetime, timedelta
from alembic import op as operation
from sqlalchemy import extract
from sqlalchemy.sql import functions

def get_all_tests_for_batch_by_day(course_id:str, batch_id:str, academic_year:str, date_of_test:date, db: Session):
    
    result = db.query(models.TestDetails).filter(
                models.TestDetails.academic_year == academic_year
            ).filter(
                models.TestDetails.course_id == course_id
            ).filter(
                models.TestDetails.batch_id == batch_id
            ).filter(
                models.TestDetails.date_of_test == date_of_test
            ).all()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    
    return result 

def get_all_tests_for_batch_by_subject(course_id:str, batch_id:str, academic_year:str, subject_id:str, db: Session):
    
    result = db.query(models.TestDetails).filter(
                models.TestDetails.academic_year == academic_year
            ).filter(
                models.TestDetails.course_id == course_id
            ).filter(
                models.TestDetails.batch_id == batch_id
            ).filter(
                models.TestDetails.subject_id == subject_id
            ).all()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    
    return result     

def get_all_tests_for_batch_by_subject_and_date(course_id:str, batch_id:str, academic_year:str, subject_id:str, date_of_test:date, db: Session):
    
    result = db.query(models.TestDetails).filter(
                models.TestDetails.academic_year == academic_year
            ).filter(
                models.TestDetails.course_id == course_id
            ).filter(
                models.TestDetails.batch_id == batch_id
            ).filter(
                models.TestDetails.subject_id == subject_id
            ).filter(
                models.TestDetails.date_of_test == date_of_test
            ).all()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    
    return result  

def upload_test_details(request: List[schemas.TestDetails], db: Session):
    records = list()
    
    for record in request:
        record_dict = record.dict()
        record_dict["test_id"] = record.get("subject_id", "subject_id") + record.get("date_of_test", datetime.today().strftime('%Y%m%d')) + record.get("time_of_test", datetime.now().strftime('%H%M%S'))
        records.append(record_dict)
    
    db.bulk_update_mappings(models.TestDetails, records)
    db.commit()

    return "Updated successfully" 