from datetime import date
from typing import List, Optional

from humps import camelize
from pydantic import BaseModel

class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class TestDetails(CamelModel):
    course_id : str
    batch_id : str
    academic_year : str
    staff_id : str
    subject_id : str
    time_of_test : str
    date_of_test : date
    marks : float
    syllabus : str
    class Config:
        orm_mode=True