from fastapi import APIRouter, Depends, status
from academic.test_module.test_questions.router import test_questions_router

from typing import List
from utils import database
from utils.database import get_db
from sqlalchemy.orm import Session
from datetime import date


from academic.test_module import models, schemas, test_module

test_module_router = APIRouter(tags=['Academic - Test Module'], prefix='/test_module')

test_module_router.include_router(test_questions_router)

@test_module_router.get('/{course_id}/{batch_id}/{academic_year}/{date_of_test}', status_code=status.HTTP_200_OK)
def get_all_tests_for_batch_by_day(course_id:str, batch_id:str, academic_year:str, date_of_test:date, db: Session= Depends(get_db)):
    
    return get_all_tests_for_batch_by_day(course_id, batch_id, academic_year, date_of_test, db)

@test_module_router.get('/{course_id}/{batch_id}/{academic_year}/{subject_id}', status_code=status.HTTP_200_OK)
def get_all_tests_for_batch_by_subject(course_id:str, batch_id:str, academic_year:str, subject_id:str, db: Session= Depends(get_db)):
    
    return get_all_tests_for_batch_by_subject(course_id, batch_id, academic_year, subject_id, db)

@test_module_router.get('/{course_id}/{batch_id}/{academic_year}/{subject_id}/{date_of_test}', status_code=status.HTTP_200_OK)
def get_all_tests_for_batch_by_subject_and_date(course_id:str, batch_id:str, academic_year:str, subject_id:str, date_of_test:date, db: Session= Depends(get_db)):
    
    return get_all_tests_for_batch_by_subject_and_date(course_id, batch_id, academic_year, subject_id, date_of_test, db)


@test_module_router.post('/test_details', status_code=status.HTTP_201_CREATED)
def upload_test_details(request: List[schemas.TestDetails], db: Session = Depends(get_db)):
    """
    Upload test details
    """
    return test_module.upload_test_details(request, db)