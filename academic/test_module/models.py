from sqlalchemy import Boolean, Column, Integer, String, true
from sqlalchemy.sql.schema import ForeignKeyConstraint, PrimaryKeyConstraint
from sqlalchemy.sql.sqltypes import DATE, Date, Float

from utils.database import Base

class TestDetails(Base):
    __tablename__ = 'TestDetails'

    course_id = Column('CourseID', String(5))
    batch_id = Column('BatchID', String(20))
    academic_year = Column('AcademicYear', String(10))
    staff_id = Column('StaffID', String(20))
    subject_id = Column('SubjectID', String(20))
    test_id = Column('TestID', String(20))
    time_of_test = Column('TimeOfTest', String(20))
    date_of_test = Column('DateOfTest', Date)
    marks = Column('Marks', Float)
    syllabus = Column('Syllabus', String(200))

    PrimaryKeyConstraint(test_id)