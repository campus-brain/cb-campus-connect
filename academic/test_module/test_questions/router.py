from fastapi import APIRouter, Depends, status

from academic.test_module.test_questions import models, schemas, test_questions

test_questions_router = APIRouter(prefix='/test_questions')


@test_questions_router.get('/{test_id}', status_code=status.HTTP_200_OK)
def get_test_questions(test_id:str):
    """
    Returns the test questions
    """
    return test_questions.get_test_question(test_id)

@test_questions_router.post('/{test_id}', status_code=status.HTTP_201_CREATED)
def upload_test_questions(test_id, request):
    """
    Upload test details
    """
    return test_questions.upload_test_questions(request, test_id)