import boto3
from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError
from fastapi import HTTPException, status

db_client = boto3.resource('dynamodb')

def get_test_question(test_id:str):
    table = db_client.Table("srkm_test_questions")
    try:
        response = table.get_item(Key={'test_id':test_id})
    except ClientError as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Error while retrieving data")
    else:
        if response.get('Item', False) == False:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
        return response['Item']

def upload_test_questions(request, test_id):
    table = db_client.Table("srkm_test_questions")
    try:
        response = table.put_item(
            Item = {
                'TestID': test_id,
                'Questions': request['Questions']
            }
        )
    except ClientError as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Error while uploading data")
    else:
        return response