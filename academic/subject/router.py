from typing import List
from fastapi import APIRouter, Depends, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from academic.subject import models, schemas, subject
from utils import database
from utils.database import get_db

subject_router = APIRouter(tags=['Academic - Subject'], prefix='/subject')


@subject_router.post('/create', status_code=status.HTTP_201_CREATED)
def create_subject(request: schemas.Subject, db: Session = Depends(get_db)):
    """
    Create Subject
    """
    return subject.create(request, db)

@subject_router.post('/subject/offered', status_code=status.HTTP_201_CREATED)
def add_subject_offered(request: schemas.SubjectsOffered, db: Session = Depends(get_db)):
    """
    Add subject offered
    """
    return subject.add_subject_offered(request, db)

@subject_router.post('/subject/batch', status_code=status.HTTP_201_CREATED)
def assign_default_subject_to_batch(request: List[schemas.SubjectForBatch], db: Session = Depends(get_db)):
    """
    Assign default subject to batch
    """
    return subject.assign_default_subject_to_batch(request, db)

@subject_router.get('/', status_code=status.HTTP_201_CREATED)
def get_all_subjects(db: Session = Depends(get_db)):
    """
    Get all subjects
    """
    return subject.get_all_subjects(db)
