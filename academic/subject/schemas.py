from typing import List, Optional

from humps import camelize
from pydantic import BaseModel

class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class Subject(CamelModel):
    subject_id: str
    subject_name: str
    class Config:
        orm_mode=True

class SubjectsOffered(CamelModel):
    subject_id: str
    course_id: str
    subject_type: str
    academic_year: str
    class Config:
        orm_mode=True

class SubjectForBatch(CamelModel):
    subject_id: str
    course_id: str
    batch_id: str
    academic_year: str
    subject_type: str
    class Config:
        orm_mode=True