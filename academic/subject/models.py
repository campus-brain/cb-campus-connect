from sqlalchemy import Boolean, Column, Integer, String, true
from sqlalchemy.sql.schema import ForeignKeyConstraint, PrimaryKeyConstraint

from utils.database import Base


class Subject(Base):
    __tablename__ = 'Subject'

    subject_id = Column('SubjectID', String(10))
    subject_name = Column('SubjectName', String(20))
    PrimaryKeyConstraint(subject_id)

class SubjectsOffered(Base):
    __tablename__ = 'SubjectsOffered'

    subject_id = Column('SubjectID', String(10))
    course_id = Column('CourseID', String(5))
    subject_type = Column('SubjectType', String(20))
    academic_year = Column('AcademicYear', String(10))
    PrimaryKeyConstraint(subject_id, course_id)

class SubjectForBatch(Base):
    __tablename__ = 'SubjectForBatch'

    subject_id = Column('SubjectID', String(10))
    course_id = Column('CourseID', String(5))
    batch_id = Column('BatchID', String(10))
    subject_type = Column('SubjectType', String(20))
    academic_year = Column('AcademicYear', String(10))
    PrimaryKeyConstraint(subject_id, course_id)


