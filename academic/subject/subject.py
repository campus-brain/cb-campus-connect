from typing import List
from fastapi import HTTPException, status
from sqlalchemy.orm import Session

from academic.subject import schemas, models
from academic.batch.models import StudentBatchTracker
from utils.hash import Hash


def create(request: schemas.Subject, db: Session):
    new_subject = models.Subject(
        subject_id = request.subject_id, subject_name = request.subject_name, 
        active=True)
    db.add(new_subject)
    db.commit()
    db.refresh(new_subject)
    return new_subject

def add_subject_offered(request: schemas.SubjectsOffered, db: Session):
    new_subject_offered = models.SubjectsOffered(
        subject_id = request.subject_id, course_id = request.course_id, subject_type = request.subject_type, 
        active=True)
    db.add(new_subject_offered)
    db.commit()
    db.refresh(new_subject_offered)
    return new_subject_offered

def assign_default_subject_to_batch(request: List[schemas.SubjectForBatch], db: Session):
    subject_batch_records = list()
    for record in request:
        subject_batch_record = models.SubjectForBatch(
            course_id = record.course_id, batch_id = record.batch_id, 
            academic_year = record.academic_year,  subject_id = record.subject_id,
            subject_type = record.subject_type)
        subject_batch_records.append(subject_batch_record)

    db.add_all(subject_batch_records)
    db.commit()
    
    
    result = db.query(models.SubjectForBatch, StudentBatchTracker).filter(
                models.SubjectForBatch.academic_year == StudentBatchTracker.academic_year
            ).filter(
                models.SubjectForBatch.course_id == record.course_id
            ).filter(
                models.SubjectForBatch.batch_id == record.batch_id
            ).all()
    
    print(result)
    return "Updated successfully" 
    

def get_all_subjects(db: Session):
    return {"subjects": db.query(models.Subject).all()} 

