from typing import List
from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from sqlalchemy.sql.elements import or_

from academic.staff_subject import schemas, models
from utils.hash import Hash


def get_staff_and_subject_by_batch(course_id, batch_id, academic_year, db:Session):
    result = db.query(models.StaffSubjectTracker).filter(
                models.StaffSubjectTracker.academic_year == academic_year
            ).filter(
                models.StaffSubjectTracker.course_id == course_id
            ).filter(or_(
                models.StaffSubjectTracker.batch_id == batch_id,
                models.StaffSubjectTracker.batch_id == "GENERAL")
            ).all()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    return result

def add_staff_and_subject_to_batch(request: List[schemas.StaffSubjectTracker], db:Session):
    records = list()
    for record in request:
        staff_subject_record = models.StaffSubjectTracker(
            course_id = record.course_id, batch_id = record.batch_id, 
            academic_year = record.academic_year,  staff_id = record.staff_id,
            subject_id = record.subject_id)
        records.append(staff_subject_record)

    db.add_all(records)
    db.commit()
    
    return "Updated successfully" 

def update_staff_and_subject_to_batch(request: List[schemas.StaffSubjectTracker], db:Session):
    
    records = list()
    
    for record in request:
        records.append(record.dict())
    
    db.bulk_update_mappings(models.StaffSubjectTracker, records)
    db.commit()
    return "Updated successfully"

def get_potential_subjects_for_staff(staff_id, db: Session):
    result = db.query(models.StaffForSubjects).filter(
                models.StaffForSubjects.staff_id == staff_id
            ).all()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    return result

def get_potential_staffs_for_subject(subject_id, db: Session):
    result = db.query(models.StaffForSubjects).filter(
                models.StaffForSubjects.subject_id == subject_id
            ).all()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    return result

def add_staff_to_subject(request: List[schemas.StaffForSubjects], db: Session):
    records = list()
    for record in request:
        staff_subject_record = models.StaffForSubjects(
             staff_id = record.staff_id,
            subject_id = record.subject_id)
        records.append(staff_subject_record)

    db.add_all(records)
    db.commit()
    
    return "Updated successfully" 

def update_staff_to_subject(request: List[schemas.StaffForSubjects], db:Session):
    
    records = list()
    
    for record in request:
        records.append(record.dict())
    
    db.bulk_update_mappings(models.StaffForSubjects, records)
    db.commit()
    return "Updated successfully"
