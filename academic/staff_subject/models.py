from sqlalchemy import Boolean, Column, Integer, String, true
from sqlalchemy.sql.schema import ForeignKeyConstraint, PrimaryKeyConstraint

from utils.database import Base


class StaffSubjectTracker(Base):
    __tablename__ = 'StaffSubjectTracker'

    staff_id = Column('StaffID', String(20))
    subject_id = Column('SubjectID', String(10))
    course_id = Column('CourseID', String(10))
    batch_id = Column('BatchID', String(10))
    academic_year = Column('AcademicYear', String(10))

    PrimaryKeyConstraint(staff_id, subject_id, course_id, batch_id, academic_year)
    
class StaffForSubjects(Base):
    __tablename__ = 'StaffForSubjects'

    staff_id = Column('StaffID', String(20))
    subject_id = Column('SubjectID', String(10))
    PrimaryKeyConstraint(staff_id, subject_id)

