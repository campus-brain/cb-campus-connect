from typing import List
from fastapi import APIRouter, Depends, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from academic.staff_subject import models, schemas, staff_subject
from utils import database
from utils.database import get_db

staff_subject_router = APIRouter(tags=['Academic - Staff - Subject'], prefix='/subject-staff')


@staff_subject_router.get('/tracker/{course_id}/{batch_id}/{academic_year}', status_code=status.HTTP_200_OK)
def get_staff_and_subject_by_batch(course_id, batch_id, academic_year, db: Session = Depends(get_db)):
    """
    
    """
    return staff_subject.get_staff_and_subject_by_batch(course_id, batch_id, academic_year, db)

@staff_subject_router.post('/tracker/', status_code=status.HTTP_201_CREATED)
def add_staff_and_subject_to_batch(request: List[schemas.StaffSubjectTracker], db: Session = Depends(get_db)):
    """
    Assign a list of staffs to the respective subjects for a batch
    """
    return staff_subject.add_staff_and_subject_to_batch(request, db)

@staff_subject_router.patch('/tracker/', status_code=status.HTTP_201_CREATED)
def update_staff_and_subject_to_batch(request: List[schemas.StaffSubjectTracker], db: Session = Depends(get_db)):
    """
    Update and assign a list of staffs to the respective subjects for a batch
    """
    return staff_subject.update_staff_and_subject_to_batch(request, db)

@staff_subject_router.get('/potential/{staff_id}/', status_code=status.HTTP_200_OK)
def get_potential_subjects_for_staff(staff_id, db: Session = Depends(get_db)):
    """
    
    """
    return staff_subject.get_potential_subjects_for_staff(staff_id, db)

@staff_subject_router.get('/potential/{subject_id}/', status_code=status.HTTP_200_OK)
def get_potential_staffs_for_subject(subject_id, db: Session = Depends(get_db)):
    """
    
    """
    return staff_subject.get_potential_staffs_for_subject(subject_id, db)

@staff_subject_router.post('/potential/', status_code=status.HTTP_201_CREATED)
def add_staff_to_subject(request: List[schemas.StaffForSubjects], db: Session = Depends(get_db)):
    """
    Assign a list of staffs to the respective subjects
    """
    return staff_subject.add_staff_to_subject(request, db)

@staff_subject_router.patch('/potential/', status_code=status.HTTP_201_CREATED)
def update_staff_to_subject(request: List[schemas.StaffForSubjects], db: Session = Depends(get_db)):
    """
    Update and assign a list of staffs to the respective subjects
    """
    return staff_subject.update_staff_to_subject(request, db)