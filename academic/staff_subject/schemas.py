from typing import List, Optional

from humps import camelize
from pydantic import BaseModel

class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class StaffSubjectTracker(CamelModel):
    course_id: str
    batch_id: str
    staff_id: str
    subject_id: str
    academic_year: str
    class Config:
        orm_mode=True

class StaffForSubjects(CamelModel):
    staff_id: str
    subject_id: str
    class Config:
        orm_mode=True
