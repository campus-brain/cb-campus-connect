from sqlalchemy import Boolean, Column, Integer, String, true
from sqlalchemy.sql.schema import ForeignKeyConstraint, PrimaryKeyConstraint

from utils.database import Base


class AcademicYear(Base):
    __tablename__ = 'AcademicYear'

    academic_year = Column('AcademicYear', String(10))
    academic_year_status = Column('AcademicYearStatus', String(5))

    PrimaryKeyConstraint(academic_year)
    

