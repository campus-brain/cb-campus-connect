from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from academic.academic_year import schemas, models
from utils.hash import Hash
from utils.database import add_column, has_table



def get_current_academic_year(db: Session):
    result = db.query(models.AcademicYear).filter(
                models.AcademicYear.academic_year_status == 'C'
            ).first()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    
    return result.academic_year  
  
def get_all_academic_year(db: Session):
    result = db.query(models.AcademicYear).all()
    return {"academic_years": jsonable_encoder(result)}  
  