from typing import List, Optional

from humps import camelize
from pydantic import BaseModel

class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class AttendanceConfig(CamelModel):
    start_date: str
    end_date: str

class AcademicYear(CamelModel):
    academic_year: str
    academic_year_status: int

class AcademicYearList(CamelModel):
    academic_years: List[AcademicYear]