from fastapi import APIRouter, Depends, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from academic.academic_year import models, schemas, academic_year
from utils import database
from utils.database import get_db

academic_year_router = APIRouter(tags=['Academic - Academic Year'], prefix='/academic-year')


@academic_year_router.get('/current/', status_code=status.HTTP_200_OK)
def get_current_academic_year(db: Session = Depends(get_db)):
    """
    Current Academic year
    """
    return academic_year.get_current_academic_year(db)

@academic_year_router.get('/all/', status_code=status.HTTP_200_OK, response_model=schemas.AcademicYearList)
def get_all_academic_year(db: Session = Depends(get_db)):
    """
    Get all Academic year
    """
    return academic_year.get_all_academic_year(db)

