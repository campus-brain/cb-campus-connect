from typing import List
from fastapi import HTTPException, status
from sqlalchemy.orm import Session
from sqlalchemy.sql.elements import or_

from academic.schedule_info import schemas, models
from utils.hash import Hash


def get_staff_schedule(staff_id, academic_year, db:Session):
    result = db.query(models.ScheduleInfo).filter(
                models.ScheduleInfo.academic_year == academic_year
            ).filter(
                models.ScheduleInfo.staff_id == staff_id
            ).all()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    return result

def get_batch_schedule(course_id, batch_id, academic_year, db:Session):
    result = db.query(models.ScheduleInfo).filter(
                models.ScheduleInfo.academic_year == academic_year
            ).filter(
                models.ScheduleInfo.course_id == course_id
            ).filter(
                models.ScheduleInfo.batch_id == batch_id
            ).all()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    return result

def add_schedule(request: List[schemas.ScheduleInfo], db:Session):
    records = list()
    for record in request:
        records.append(record.dict())
    
    db.bulk_insert_mappings(models.ScheduleInfo, records)
    db.commit()
    
    return "Updated successfully" 

def update_schedule(request: List[schemas.ScheduleInfo], db:Session):
    
    records = list()
    
    for record in request:
        records.append(record.dict())
    
    db.bulk_update_mappings(models.ScheduleInfo, records)
    db.commit()
    return "Updated successfully"

def get_period( period_id, day_of_week, academic_year, db: Session):
    result = db.query(models.PeriodInfo).filter(
                models.PeriodInfo.academic_year == academic_year
            ).filter(
                models.PeriodInfo.period_id == period_id
            ).filter(
                models.ScheduleInfo.day_of_week == day_of_week
            ).all()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    return result

def get_period_by_day(day_of_week, academic_year, db: Session):
    result = db.query(models.PeriodInfo).filter(
                models.PeriodInfo.academic_year == academic_year
            ).filter(
                models.ScheduleInfo.day_of_week == day_of_week
            ).all()
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No record found")
    return result

def add_period(request:List[schemas.PeriodInfo], db:Session):
    records = list()
    for record in request:
        records.append(record.dict())
    
    db.bulk_insert_mappings(models.PeriodInfo, records)
    db.commit()
    
    return "Updated successfully" 