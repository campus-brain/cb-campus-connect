from typing import List
from fastapi import APIRouter, Depends, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from academic.schedule_info import models, schemas, schedule_info
from utils import database
from utils.database import get_db

schedule_info_router = APIRouter(tags=['Academic - Schedule'], prefix='/schedule')


@schedule_info_router.get('/{staff_id}/{academic_year}', status_code=status.HTTP_200_OK)
def get_staff_schedule( staff_id, academic_year, db: Session = Depends(get_db)):
    """
    
    """
    return schedule_info.get_staff_schedule(staff_id, academic_year, db)

@schedule_info_router.get('/{course_id}/{batch_id}/{academic_year}', status_code=status.HTTP_200_OK)
def get_batch_schedule( course_id, batch_id, academic_year, db: Session = Depends(get_db)):
    """
    
    """
    return schedule_info.get_batch_schedule(course_id, batch_id, academic_year, db)

@schedule_info_router.post('/', status_code=status.HTTP_201_CREATED)
def add_schedule(request: List[schemas.ScheduleInfo], db: Session = Depends(get_db)):
    """
    Assign a list of staffs to the respective subjects for a batch
    """
    return schedule_info.add_schedule(request, db)

@schedule_info_router.patch('/', status_code=status.HTTP_201_CREATED)
def update_schedule(request: List[schemas.ScheduleInfo], db: Session = Depends(get_db)):
    """
    Update and assign a list of staffs to the respective subjects for a batch
    """
    return schedule_info.update_schedule(request, db)

@schedule_info_router.get('/period/{period_id}/{day_of_week}/{academic_year}', status_code=status.HTTP_200_OK)
def get_period( period_id, day_of_week, academic_year, db: Session = Depends(get_db)):
    """
    
    """
    return schedule_info.get_period(period_id, day_of_week, academic_year, db)

@schedule_info_router.get('/period/{day_of_week}/{academic_year}', status_code=status.HTTP_200_OK)
def get_period_by_day( day_of_week, academic_year, db: Session = Depends(get_db)):
    """
    
    """
    return schedule_info.get_period_by_day(day_of_week, academic_year, db)

@schedule_info_router.post('/period/', status_code=status.HTTP_200_OK)
def add_period( request: List[schemas.PeriodInfo], db: Session = Depends(get_db)):
    """
    
    """
    return schedule_info.add_period(request, db)