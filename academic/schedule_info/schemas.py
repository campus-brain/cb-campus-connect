from typing import List, Optional

from humps import camelize
from pydantic import BaseModel

class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class PeriodInfo(CamelModel):
    period_id: str
    day_of_week: str
    start_time: str
    end_time: str
    academic_year: str
    class Config:
        orm_mode=True

class ScheduleInfo(CamelModel):
    course_id: str
    batch_id: str
    academic_year: str
    period_id: str
    staff_id: str
    subject_id: str
    day_of_week: str
    class Config:
        orm_mode=True
