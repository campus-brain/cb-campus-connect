from sqlalchemy import Boolean, Column, Integer, String, true
from sqlalchemy.sql.schema import ForeignKeyConstraint, PrimaryKeyConstraint

from utils.database import Base


class PeriodInfo(Base):
    __tablename__ = 'PeriodInfo'

    period_id = Column('PeriodID', String(20))
    day_of_week = Column('DayOfWeek', String(15))
    start_time = Column('StartTime', String(10))
    end_time = Column('EndTime', String(10))
    academic_year = Column('AcademicYear', String(10))

    PrimaryKeyConstraint(period_id, day_of_week)
    
class ScheduleInfo(Base):
    __tablename__ = 'ScheduleInfo'

    course_id = Column('CourseID', String(20))
    batch_id = Column('BatchID', String(10))
    academic_year = Column('AcademicYear', String(10))
    period_id = Column("PeriodID", String(20))
    staff_id = Column("StaffID", String(20))
    subject_id = Column("SubjectID", String(15))
    day_of_week = Column("DayOfWeek", String(15))
    PrimaryKeyConstraint(course_id, batch_id, academic_year, period_id, day_of_week, subject_id)

