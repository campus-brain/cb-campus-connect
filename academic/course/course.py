from fastapi import HTTPException, status
from sqlalchemy.orm import Session

from academic.course import schemas, models
from utils.hash import Hash


def create(request: schemas.Course, db: Session):
    new_batch = models.Course(
        course_id = request.course_id, course_description = request.course_description, 
        active=True)
    db.add(new_batch)
    db.commit()
    db.refresh(new_batch)
    return new_batch

def get_all_courses(db: Session):
    return {"courses": db.query(models.Course).all()} 

