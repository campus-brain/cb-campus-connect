from sqlalchemy import Boolean, Column, Integer, String, true
from sqlalchemy.sql.schema import ForeignKeyConstraint, PrimaryKeyConstraint

from utils.database import Base


class Course(Base):
    __tablename__ = 'Course'

    course_id = Column('CourseID', String(5))
    course_description = Column('CourseDescription', String(20))
    PrimaryKeyConstraint(course_id)

