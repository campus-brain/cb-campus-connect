from fastapi import APIRouter, Depends, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from academic.course import models, schemas, course
from utils import database
from utils.database import get_db

course_router = APIRouter(tags=['Academic - Course'], prefix='/course')


@course_router.post('/create', status_code=status.HTTP_201_CREATED)
def create_course(request: schemas.Course, db: Session = Depends(get_db)):
    """
    Create Course
    """
    return course.create(request, db)


@course_router.get('/', status_code=status.HTTP_201_CREATED)
def get_all_courses(db: Session = Depends(get_db)):
    """
    Get all courses
    """
    return course.get_all_courses(db)
