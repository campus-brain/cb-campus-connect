from typing import List, Optional

from humps import camelize
from pydantic import BaseModel



class CamelModel(BaseModel):
    class Config:
        alias_generator = camelize
        allow_population_by_field_name = True

class Course(CamelModel):
    course_id: str
    course_description: str
    class Config:
        orm_mode=True
